package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.JobController;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BadWordResourceAssembler
    implements RepresentationModelAssembler<BadWordDto, EntityModel<BadWordDto>> {

  @Override
  public EntityModel<BadWordDto> toModel(BadWordDto entity) {
    try {
      return EntityModel.of(
          entity,
          linkTo(methodOn(JobController.class).show(entity.getId())).withSelfRel(),
          linkTo(methodOn(JobController.class).list(null, "", "", "")).withRel("jobs"));

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
