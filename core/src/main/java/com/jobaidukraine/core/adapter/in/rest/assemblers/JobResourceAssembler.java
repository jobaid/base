package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.JobController;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JobResourceAssembler
    implements RepresentationModelAssembler<JobDto, EntityModel<JobDto>> {

  @Override
  public EntityModel<JobDto> toModel(JobDto job) {

    try {
      return EntityModel.of(
          job,
          linkTo(methodOn(JobController.class).show(job.getId())).withSelfRel(),
          linkTo(methodOn(JobController.class).list(null, "", "", "")).withRel("jobs"));

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
