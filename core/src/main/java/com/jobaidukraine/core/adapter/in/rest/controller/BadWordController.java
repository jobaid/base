package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.BadWordResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.BadWordMapper;
import com.jobaidukraine.core.services.ports.in.BadWordUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/v1.0/bad-words")
@RestController
public class BadWordController {
  private final BadWordUseCase badWordUseCase;
  private final PagedResourcesAssembler<BadWordDto> pagedResourcesAssembler;
  private final BadWordResourceAssembler badWordResourceAssembler;
  private final BadWordMapper badWordMapper;

  @Operation(
      summary = "Create a new bad word. This only works with the JWT token set.",
      tags = {"BadWord"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Bad word creation was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = BadWordDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid bad word supplied")
      })
  @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR')")
  @PostMapping
  public EntityModel<BadWordDto> save(@Valid @RequestBody BadWordDto badWord) {
    badWord = badWordMapper.toDto(badWordUseCase.saveBadWord(badWordMapper.toDomain(badWord)));
    return badWordResourceAssembler.toModel(badWord);
  }

  @Operation(
      summary = "Get all bad words",
      tags = {"BadWord"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Bad word retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = BadWordDto.class)))),
        @ApiResponse(responseCode = "400", description = "Invalid pageable supplied"),
        @ApiResponse(responseCode = "404", description = "No bad words found")
      })
  @GetMapping
  public PagedModel<EntityModel<BadWordDto>> list(Pageable pageable) {
    Page<BadWordDto> badWords = badWordUseCase.findAll(pageable).map(badWordMapper::toDto);
    return pagedResourcesAssembler.toModel(badWords, badWordResourceAssembler);
  }

  @Operation(
      summary = "Get bad word by id",
      tags = {"BadWord"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Bad word retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = BadWordDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "404", description = "Bad word not found")
      })
  @GetMapping(value = "/{id}")
  public EntityModel<BadWordDto> show(@PathVariable String id) {
    BadWordDto badWord = badWordMapper.toDto(badWordUseCase.findById(id));
    return badWordResourceAssembler.toModel(badWord);
  }

  @Operation(
      summary = "Update a bad word",
      tags = {"BadWord"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Bad word update was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = BadWordDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid bad word or id supplied"),
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR') or hasAuthority('ROLE_EMPLOYER')")
  public EntityModel<BadWordDto> update(
      @Valid @RequestBody BadWordDto badWord, @PathVariable String id) {
    badWord.setId(id);
    badWord =
        badWordMapper.toDto(this.badWordUseCase.updateBadWord(badWordMapper.toDomain(badWord)));
    return badWordResourceAssembler.toModel(badWord);
  }

  @Operation(
      summary = "Delete a bad word",
      tags = {"BadWord"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Bad word deletion was successful"),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
      })
  @DeleteMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR') or hasAuthority('ROLE_EMPLOYER')")
  public void delete(@PathVariable String id) {
    this.badWordUseCase.deleteBadWord(id);
  }
}
