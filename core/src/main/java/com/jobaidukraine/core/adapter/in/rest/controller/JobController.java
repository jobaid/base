package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.JobResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.JobMapper;
import com.jobaidukraine.core.services.ports.in.JobUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/jobs")
@RequiredArgsConstructor
public class JobController {

  private final JobUseCase jobUseCase;
  private final PagedResourcesAssembler<JobDto> pagedResourcesAssembler;
  private final JobResourceAssembler jobResourceAssembler;
  private final JobMapper jobMapper;

  @Operation(
      summary = "Create a new job for a given company. This only works with the JWT token set.",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job creation was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid job supplied")
      })
  @PreAuthorize("hasAuthority('ROLE_EMPLOYER')")
  @PostMapping
  public EntityModel<JobDto> save(
      @Valid @RequestBody JobDto job, @AuthenticationPrincipal Jwt principal) {
    var employerId = principal.getClaimAsString("userId");
    job = jobMapper.toDto(jobUseCase.save(jobMapper.toDomain(job), employerId));
    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Get all jobs",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = JobDto.class))))
      })
  @GetMapping
  public PagedModel<EntityModel<JobDto>> list(
      @Parameter(name = "pageable", schema = @Schema(example = "{\"page\": 0, \"size\": 1}"))
          Pageable pageable,
      @RequestParam(required = false, name = "title") String title,
      @RequestParam(required = false, name = "description") String description,
      @RequestParam(required = false, name = "skill") String skill) {
    Page<JobDto> jobs = null;
    if (title != null && description != null && skill != null) {
      jobs =
          this.jobUseCase
              .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                  pageable, title, description, skill)
              .map(jobMapper::toDto);
    } else if (title != null && description != null) {
      jobs =
          this.jobUseCase
              .findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
                  pageable, title, description)
              .map(jobMapper::toDto);
    } else if (title != null && skill != null) {
      jobs =
          this.jobUseCase
              .findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
                  pageable, title, skill)
              .map(jobMapper::toDto);
    } else if (description != null && skill != null) {
      jobs =
          this.jobUseCase
              .findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                  pageable, description, skill)
              .map(jobMapper::toDto);
    } else if (title != null) {
      jobs =
          this.jobUseCase
              .findAllByTitlesValueContainingAndNoBadWords(pageable, title)
              .map(jobMapper::toDto);
    } else if (description != null) {
      jobs =
          this.jobUseCase
              .findAllByDescriptionsValueContainingAndNoBadWords(pageable, description)
              .map(jobMapper::toDto);
    } else if (skill != null) {
      jobs =
          this.jobUseCase
              .findAllBySkillsNameContainingAndNoBadWords(pageable, skill)
              .map(jobMapper::toDto);
    } else {
      jobs = this.jobUseCase.findAllByPageableAndNoBadWords(pageable).map(jobMapper::toDto);
    }
    return pagedResourcesAssembler.toModel(jobs, jobResourceAssembler);
  }

  @Operation(
      summary = "Get all jobs that need moderation",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = JobDto.class)))),
        @ApiResponse(responseCode = "400", description = "Invalid pageable supplied"),
        @ApiResponse(responseCode = "404", description = "No job found")
      })
  @GetMapping("/moderation")
  public PagedModel<EntityModel<JobDto>> listWithBadWords(
      @Parameter(name = "pageable", schema = @Schema(example = "{\"page\": 0, \"size\": 1}"))
          Pageable pageable) {
    Page<JobDto> jobs = jobUseCase.findAllByPageableWithBadWords(pageable).map(jobMapper::toDto);
    return pagedResourcesAssembler.toModel(jobs, jobResourceAssembler);
  }

  @Operation(
      summary = "Get job by id",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "404", description = "Job not found")
      })
  @GetMapping(value = "/{id}")
  public EntityModel<JobDto> show(@PathVariable String id) {
    JobDto job = jobMapper.toDto(this.jobUseCase.findById(id));
    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Update a job",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job update was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid job or id supplied"),
        @ApiResponse(responseCode = "404", description = "Job not found")
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR') or hasAuthority('ROLE_EMPLOYER')")
  public EntityModel<JobDto> update(@Valid @RequestBody JobDto job, @PathVariable String id) {
    job.setId(id);
    job = jobMapper.toDto(this.jobUseCase.update(jobMapper.toDomain(job)));

    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Delete a job",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "Job deletion was successful"),
        @ApiResponse(responseCode = "404", description = "No Job found to be deleted"),
      })
  @DeleteMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_MODERATOR') or hasAuthority('ROLE_EMPLOYER')")
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void delete(@PathVariable String id) {
    this.jobUseCase.delete(id);
  }
}
