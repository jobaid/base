package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Address", description = "An address for companys, jobs, housing, etc.")
public class AddressDto {

  @Min(value = -90, message = "Latitude must be between -90 and 90")
  @Max(value = 90, message = "Latitude must be between -90 and 90")
  @Schema(description = "The latitude coordinate of the address", example = "51.510967")
  private Float latitude;

  @Min(value = -180, message = "Longitude must be between -180 and 180")
  @Max(value = 180, message = "Longitude must be between -180 and 180")
  @Schema(description = "The longitude coordinate of the address", example = "7.466081")
  private Float longitude;

  @NotBlank(message = "The address street may not be blank or empty.")
  @Schema(description = "The street of the address", required = true, example = "Friedensplatz")
  private String street;

  @NotBlank(message = "The address house number may not be blank or empty.")
  @Schema(description = "The house number of the address", required = true, example = "1337")
  private String houseNumber;

  @NotNull(message = "The address city may not be null.")
  @Schema(description = "The translated city of the address", required = true)
  private Set<TranslationDto> cityTranslations;

  @NotBlank(
      message =
          "The address ZIP code is valid as long as it's not null, and the trimmed length is greater than zero.")
  @Schema(description = "The ZIP code of the address", required = true, example = "44135")
  private String zip;

  @NotBlank(
      message =
          "The address country is valid as long as it's not null, and the trimmed length is greater than zero.")
  @Schema(description = "The country of the address", required = true, example = "DE")
  private String country;
}
