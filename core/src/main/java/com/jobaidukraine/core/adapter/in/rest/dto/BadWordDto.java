package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "BadWord", description = "A bad word that is not allowed in job descriptions.")
public class BadWordDto {

  @Schema(
      description =
          "The database generated bad word ID. "
              + "This hast to be null when creating a new bad word. "
              + "This has to be set when updating an existing bad word.",
      example = "744e1871-239b-485b-b4b0-9ae8d124affc")
  private String id;

  @Valid
  @NotEmpty(message = "The bad word must be given in at least one language.")
  private Set<TranslationDto> wordTranslations;
}
