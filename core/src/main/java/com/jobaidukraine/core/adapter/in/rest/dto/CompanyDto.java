package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Company", description = "A company that provides jobs")
public class CompanyDto {

  @NotBlank(
      message =
          "The company name is valid as long as it's not null, and the trimmed length is greater than zero.")
  @Schema(description = "The company name.")
  private String name;

  @Pattern(
      regexp =
          "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&/=]*)",
      message = "The company website URL is not valid.")
  @Schema(description = "The company url that refers to the official website of the company.")
  private String url;

  @Email(message = "The company email is not valid.")
  @NotBlank(
      message =
          "The company email is valid as long as it's not null, and the trimmed length is greater than zero.")
  @Schema(description = "The company email.")
  private String email;

  @Schema(description = "The company logo.")
  private String logo;
}
