package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class EmployerDto extends UserDto {

  @Schema(description = "The user related company.", example = "Adesso")
  private CompanyDto company;

  @Schema(
      description = "The user related job offers.",
      example = "[Software Developer, Data Scientist]")
  private Set<JobDto> jobs;
}
