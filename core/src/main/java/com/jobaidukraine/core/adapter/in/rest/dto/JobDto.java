package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class JobDto extends AuditableDto {
  @Schema(
      description =
          "The database generated job ID. "
              + "This hast to be null when creating a new job. "
              + "This has to be set when updating an existing job.",
      example = "744e1871-239b-485b-b4b0-9ae8d124affc")
  private String id;

  @Valid
  @Schema(description = "The address of the job offer")
  private AddressDto address;

  @NotNull(message = "The bad-word Flag may not be null.")
  @Schema(example = "false")
  private Boolean badWordFlag;

  @Valid
  @Schema(description = "The translated job titles.", required = true)
  private Set<TranslationDto> titles;

  @Valid
  @Schema(description = "The translated job descriptions.")
  private Set<TranslationDto> descriptions;

  @NotNull
  @Schema(description = "The job type.", example = "FREELANCE")
  private JobTypeDto jobType;

  @Pattern(
      regexp =
          "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&/=]*)",
      message = "The company website URL is not valid.")
  @Schema(
      description = "The application link for the job offer",
      example = "https://www.company.com/apply/1")
  private String applicationLink;

  @Email(message = "The email is not valid.")
  @Schema(description = " The application mail for the job offer", example = "apply@company.com")
  private String applicationMail;

  @Pattern(
      regexp =
          "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&/=]*)",
      message = "The company website URL is not valid.")
  @Schema(
      description =
          "The url to a promotional video for the job offer that is being embedded on the site",
      example = "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
  private String videoUrl;

  @Valid
  @NotNull(message = "The job offer may have at least one language level.")
  @Schema(description = "The language levels needed for the job offer")
  private Set<LanguageSkillDto> languages;

  @Valid
  @Schema(description = "The qualifications needed for the job offer")
  private Set<SkillDto> skills;

  @Schema(example = "744e1871-239b-485b-b4b0-9ae8d124affc")
  private String employerId;
}
