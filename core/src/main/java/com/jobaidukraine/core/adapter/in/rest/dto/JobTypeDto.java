package com.jobaidukraine.core.adapter.in.rest.dto;

public enum JobTypeDto {
  FREELANCE,
  FULL_TIME,
  PART_TIME,
  TEMPORARY
}
