package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Locale;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageSkillDto {

  @NotNull
  @Schema(description = "The language.", example = "de_DE")
  private Locale locale;

  @NotNull
  @Schema(description = "The level of a language.", example = "NATIVE")
  private LanguageLevelDto level;
}
