package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class RefugeeDto extends UserDto {

  @Valid
  @NotNull(message = "The user may have at least one language level.")
  @Schema(description = "All languages a user is able to speak at some level")
  private Set<LanguageSkillDto> languages;

  @Valid
  @NotNull(message = "The skills may not be null.")
  @Schema(description = "All Skills a user is able to supply.")
  private Set<SkillDto> skills;
}
