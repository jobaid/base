package com.jobaidukraine.core.adapter.in.rest.dto;

public enum RoleDto {
  ADMIN,
  MODERATOR,
  REFUGEE,
  EMPLOYER;
}
