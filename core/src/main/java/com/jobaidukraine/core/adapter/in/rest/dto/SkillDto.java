package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillDto {

  @NotBlank(
      message =
          "The skill name is valid as long as it's not null, and the trimmed length is greater than zero.")
  @Schema(description = "The skill name.", example = "Project management")
  String name;

  @NotNull(message = "The skill level may not be null.")
  @Schema(description = "The level of a skill.", example = "BEGINNER")
  SkillLevelDto level;
}
