package com.jobaidukraine.core.adapter.in.rest.dto;

public enum SkillLevelDto {
  BEGINNER,
  INTERMEDIATE,
  PROFESSIONAL,
}
