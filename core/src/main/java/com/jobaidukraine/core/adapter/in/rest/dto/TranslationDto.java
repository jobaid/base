package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.Locale;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TranslationDto {

  @NotNull(message = "The locale may not be null.")
  @Schema(description = "The locale key", required = true, example = "de_DE")
  private Locale locale;

  @NotBlank(message = "The translated text may not be blank or empty.")
  @Schema(
      description = "The value in the language of the provided locale",
      required = true,
      example = "Dortmund")
  private String value;

  @NotNull(message = "The translation type may not be null.")
  @Schema(
      description = "The type of the translation",
      required = true,
      example = "ORIGINAL_TRANSLATION")
  private TranslationTypeDto type;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @Schema(description = "The date of the translation", example = "2021-12-24T11:10:00.000Z")
  private LocalDateTime translatedOn;

  @NotBlank(message = "The author of the translation may not be blank or empty.")
  @Schema(
      description =
          "The author of the translation. Which might be a machine like Google Translate or a valid user email.",
      required = true,
      example = "google-translate")
  private String translatedBy;
}
