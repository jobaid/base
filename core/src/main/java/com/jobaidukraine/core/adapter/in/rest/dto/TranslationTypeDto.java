package com.jobaidukraine.core.adapter.in.rest.dto;

public enum TranslationTypeDto {
  AUTO_TRANSLATION,
  NATIVE_TRANSLATION,
  ORIGINAL_TRANSLATION,
}
