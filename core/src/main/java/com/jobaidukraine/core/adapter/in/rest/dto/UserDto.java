package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserDto extends AuditableDto {
  @Schema(
      description =
          "The database generated user ID. "
              + "This hast to be null when creating a new user. "
              + "This has to be set when updating an existing user.",
      example = "3c077ff4-07c3-4bd2-8402-ed2843b23c62")
  private String id;

  @NotNull(message = "The role may not be null.")
  @Schema(description = "The user role.", example = "ADMIN")
  private RoleDto role;

  @Valid
  @Schema(description = "The address of the user")
  private AddressDto address;

  @Email(message = "The user email is not valid.")
  @NotBlank(message = "The user email may not be blank or empty.")
  @Schema(description = "The user email.", example = "luke@skywalker.io")
  private String email;

  @NotBlank(message = "The user firstname may not be empty.")
  @Schema(description = "The user firstname.", example = "Luke")
  private String firstname;

  @NotBlank(message = "The user lastname may not be empty.")
  @Schema(description = "The user lastname.", example = "Skywalker")
  private String lastname;

  @JsonDeserialize(using = LocalDateDeserializer.class)
  @JsonSerialize(using = LocalDateSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
  @Schema(description = "The user birthday.", example = "2021-12-24")
  private LocalDate dateOfBirth;

  @NotBlank(message = "The user origin country may not be empty.")
  @Schema(description = "The user's origin country.", example = "Tatooine")
  private String originCountry;
}
