package com.jobaidukraine.core.adapter.in.rest.handler;

import java.util.NoSuchElementException;
import org.springdoc.api.ErrorMessage;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler(value = NoSuchElementException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ErrorMessage noSuchElementFoundException(NoSuchElementException exception) {
    return new ErrorMessage(exception.getMessage());
  }

  @ExceptionHandler(value = EmptyResultDataAccessException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ErrorMessage emptyResultDataAccessException(EmptyResultDataAccessException exception) {
    return new ErrorMessage(exception.getMessage());
  }

  @ExceptionHandler(value = IllegalArgumentException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorMessage illegalArgumentException(IllegalArgumentException exception) {
    return new ErrorMessage(exception.getMessage());
  }
}
