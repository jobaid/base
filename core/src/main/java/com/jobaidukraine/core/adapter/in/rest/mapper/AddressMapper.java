package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.AddressDto;
import com.jobaidukraine.core.domain.values.Address;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoAddressMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      TranslationMapper.class,
    })
public interface AddressMapper {
  Address toDomain(AddressDto addressDto);

  AddressDto toDto(Address address);
}
