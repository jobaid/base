package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDto;
import com.jobaidukraine.core.domain.values.BadWord;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoBadWordMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      TranslationMapper.class,
    })
public interface BadWordMapper {
  BadWord toDomain(BadWordDto badWordDto);

  BadWordDto toDto(BadWord badWord);

  Set<BadWord> toDomainSet(Set<BadWordDto> badWordDtos);

  Set<BadWordDto> toDtoSet(Set<BadWord> badWords);
}
