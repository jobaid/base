package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.domain.values.Company;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoCompanyMapperImpl")
public interface CompanyMapper {
  Company toDomain(CompanyDto companyDto);

  CompanyDto toDto(Company company);
}
