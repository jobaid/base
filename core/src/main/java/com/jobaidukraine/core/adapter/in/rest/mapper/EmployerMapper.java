package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.EmployerDto;
import com.jobaidukraine.core.domain.entities.Employer;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoEmployerMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      AddressMapper.class,
      SkillMapper.class,
      RoleMapper.class,
      JobMapper.class,
      CompanyMapper.class
    })
public interface EmployerMapper {
  Employer toDomain(EmployerDto employerDto);

  EmployerDto toDto(Employer employer);
}
