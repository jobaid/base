package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.domain.entities.Job;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoJobMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      LanguageSkillMapper.class,
      AddressMapper.class,
      SkillMapper.class,
      SkillLevelMapper.class,
      JobTypeMapper.class,
      TranslationMapper.class,
    })
public interface JobMapper {

  Job toDomain(JobDto jobDto);

  JobDto toDto(Job job);

  Set<Job> toDomainSet(Set<JobDto> jobDtos);

  Set<JobDto> toDtoSet(Set<Job> jobs);
}
