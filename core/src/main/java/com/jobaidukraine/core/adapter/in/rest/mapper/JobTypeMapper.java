package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.JobTypeDto;
import com.jobaidukraine.core.domain.values.JobType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoJobTypeMapperImpl")
public interface JobTypeMapper {
  JobType toDomain(JobTypeDto jobTypeDto);

  JobTypeDto toDto(JobType jobType);
}
