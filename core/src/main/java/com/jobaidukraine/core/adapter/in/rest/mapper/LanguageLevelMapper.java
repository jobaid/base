package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.LanguageLevelDto;
import com.jobaidukraine.core.domain.values.LanguageLevel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoLanguageLevelMapperImpl")
public interface LanguageLevelMapper {
  LanguageLevel toDomain(LanguageLevelDto languageLevelDto);

  LanguageLevelDto toDto(LanguageLevel languageLevel);
}
