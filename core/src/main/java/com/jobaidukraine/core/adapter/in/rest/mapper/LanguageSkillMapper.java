package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.LanguageSkillDto;
import com.jobaidukraine.core.domain.values.LanguageSkill;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoLanguageSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {LanguageLevelMapper.class})
public interface LanguageSkillMapper {
  LanguageSkill toDomain(LanguageSkillDto languageSkillDto);

  LanguageSkillDto toDto(LanguageSkill languageSkill);

  Set<LanguageSkill> toDomainSet(Set<LanguageSkillDto> languageDtos);

  Set<LanguageSkillDto> toDtoSet(Set<LanguageSkill> languageLevels);
}
