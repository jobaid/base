package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.RefugeeDto;
import com.jobaidukraine.core.domain.entities.Refugee;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoRefugeeMapper",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      AddressMapper.class,
      JobMapper.class,
      RoleMapper.class,
      LanguageSkillMapper.class,
      SkillMapper.class,
    })
public interface RefugeeMapper {
  Refugee toDomain(RefugeeDto refugeeDto);

  RefugeeDto toDto(Refugee refugee);
}
