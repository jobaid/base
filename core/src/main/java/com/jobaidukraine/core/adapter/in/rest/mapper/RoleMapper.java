package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.RoleDto;
import com.jobaidukraine.core.domain.values.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoRoleMapperImpl")
public interface RoleMapper {
  Role toDomain(RoleDto roleDto);

  RoleDto toDto(Role role);
}
