package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.SkillLevelDto;
import com.jobaidukraine.core.domain.values.SkillLevel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoSkillLevelMapperImpl")
public interface SkillLevelMapper {
  SkillLevel toDomain(SkillLevelDto skillLevelDto);

  SkillLevelDto toDto(SkillLevel skillLevel);
}
