package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.SkillDto;
import com.jobaidukraine.core.domain.values.Skill;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {SkillLevelMapper.class})
public interface SkillMapper {
  Skill toDomain(SkillDto skillDto);

  SkillDto toDto(Skill skill);

  Set<Skill> toDomainSet(Set<SkillDto> skillDtos);

  Set<SkillDto> toDtoSet(Set<Skill> skills);
}
