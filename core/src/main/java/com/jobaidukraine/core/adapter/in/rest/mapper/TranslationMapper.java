package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.TranslationDto;
import com.jobaidukraine.core.domain.values.Translation;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoTranslationMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {TranslationTypeMapper.class})
public interface TranslationMapper {
  Translation toDomain(TranslationDto translationDto);

  TranslationDto toDto(Translation translation);

  Set<Translation> toDomainSet(Set<TranslationDto> translationDtos);

  Set<TranslationDto> toDtoSet(Set<Translation> translations);
}
