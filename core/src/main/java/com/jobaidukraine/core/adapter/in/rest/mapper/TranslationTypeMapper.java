package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.TranslationTypeDto;
import com.jobaidukraine.core.domain.values.TranslationType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "DtoTranslationTypeMapperImpl")
public interface TranslationTypeMapper {
  TranslationType toDomain(TranslationTypeDto translationTypeDto);

  TranslationTypeDto toDto(TranslationType translationType);
}
