package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import com.jobaidukraine.core.domain.entities.User;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "DtoUserMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class, RoleMapper.class})
public interface UserMapper {

  User toDomain(UserDto userDto);

  UserDto toDto(User user);
}
