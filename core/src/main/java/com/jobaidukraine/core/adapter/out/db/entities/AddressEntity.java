package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import lombok.*;

@Builder
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressEntity {

  private Float latitude;
  private Float longitude;
  private String street;
  private String houseNumber;
  private String zip;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<TranslationEntity> cityTranslations;

  private String country;
}
