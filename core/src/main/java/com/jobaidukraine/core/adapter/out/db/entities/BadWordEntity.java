package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BadWordEntity {

  @Id
  @GeneratedValue
  @Column(updatable = false, nullable = false, unique = true, columnDefinition = "BINARY(16)")
  private UUID id;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<TranslationEntity> wordTranslations;
}
