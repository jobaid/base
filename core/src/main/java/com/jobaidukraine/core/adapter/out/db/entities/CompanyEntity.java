package com.jobaidukraine.core.adapter.out.db.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.*;

@Builder
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyEntity {
  @Column(name = "COMPANY_NAME")
  private String name;

  private String url;

  @Column(name = "COMPANY_EMAIL")
  private String email;

  private String logo;
}
