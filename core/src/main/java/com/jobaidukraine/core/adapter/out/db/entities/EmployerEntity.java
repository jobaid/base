package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class EmployerEntity extends UserEntity {

  @Embedded private CompanyEntity company;

  @OneToMany(mappedBy = "employerId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private Set<JobEntity> jobs;
}
