package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@SuperBuilder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JobEntity extends AuditableEntity {

  @Id
  @GeneratedValue
  @Column(updatable = false, nullable = false, unique = true, columnDefinition = "BINARY(16)")
  private UUID id;

  @Embedded private AddressEntity address;

  private Boolean badWordFlag;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<TranslationEntity> titles;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<TranslationEntity> descriptions;

  @Enumerated(EnumType.ORDINAL)
  private JobTypeEntity jobType;

  private String applicationLink;
  private String applicationMail;
  private String videoUrl;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<LanguageSkillEntity> languages;

  @ElementCollection(fetch = FetchType.LAZY)
  private Set<SkillEntity> skills;

  @Column(nullable = false, name = "employer_id", columnDefinition = "BINARY(16)")
  private UUID employerId;
}
