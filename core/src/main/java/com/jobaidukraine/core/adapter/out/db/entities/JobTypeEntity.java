package com.jobaidukraine.core.adapter.out.db.entities;

public enum JobTypeEntity {
  FREELANCE,
  FULL_TIME,
  PART_TIME,
  TEMPORARY;
}
