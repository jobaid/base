package com.jobaidukraine.core.adapter.out.db.entities;

public enum LanguageLevelEntity {
  NATIVE,
  FLUENT,
  GOOD,
  BASIC
}
