package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Locale;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.*;

@Builder
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LanguageSkillEntity {
  private Locale locale;

  @Enumerated(EnumType.ORDINAL)
  private LanguageLevelEntity level;
}
