package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class RefugeeEntity extends UserEntity {
  @ElementCollection private Set<LanguageSkillEntity> languages;
  @ElementCollection private Set<SkillEntity> skills;
}
