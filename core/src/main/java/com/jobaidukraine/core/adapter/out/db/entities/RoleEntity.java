package com.jobaidukraine.core.adapter.out.db.entities;

public enum RoleEntity {
  ADMIN,
  MODERATOR,
  REFUGEE,
  EMPLOYER;
}
