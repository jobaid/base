package com.jobaidukraine.core.adapter.out.db.entities;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.*;

@Builder
@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkillEntity {
  private String name;

  @Enumerated(EnumType.ORDINAL)
  private SkillLevelEntity level;
}
