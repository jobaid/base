package com.jobaidukraine.core.adapter.out.db.entities;

public enum SkillLevelEntity {
  BEGINNER,
  INTERMEDIATE,
  PROFESSIONAL,
}
