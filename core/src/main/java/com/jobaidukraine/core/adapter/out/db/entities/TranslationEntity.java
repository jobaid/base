package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDateTime;
import java.util.Locale;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.*;

@Embeddable
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TranslationEntity {

  private Locale locale;

  private String value;

  @Enumerated(EnumType.ORDINAL)
  private TranslationTypeEntity type;

  private LocalDateTime translatedOn;

  private String translatedBy;
}
