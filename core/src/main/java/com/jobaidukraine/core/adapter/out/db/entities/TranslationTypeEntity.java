package com.jobaidukraine.core.adapter.out.db.entities;

public enum TranslationTypeEntity {
  AUTO_TRANSLATION,
  NATIVE_TRANSLATION,
  ORIGINAL_TRANSLATION,
}
