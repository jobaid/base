package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDate;
import java.util.UUID;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity extends AuditableEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(unique = true, columnDefinition = "BINARY(16)")
  private UUID id;

  @Enumerated(EnumType.ORDINAL)
  private RoleEntity role;

  @Embedded private AddressEntity address;

  private String email;
  private String firstname;
  private String lastname;
  private LocalDate dateOfBirth;
  private String originCountry;
}
