package com.jobaidukraine.core.adapter.out.db.implementation.badword;

import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.BadWordMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.TranslationMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.BadWordRepository;
import com.jobaidukraine.core.domain.values.BadWord;
import com.jobaidukraine.core.services.ports.out.BadWordPort;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BadWordAdapter implements BadWordPort {

  private final BadWordRepository badWordRepository;
  private final BadWordMapper badWordMapper;
  private final TranslationMapper translationMapper;

  @Override
  public Optional<BadWord> findById(String uuid) {
    return badWordRepository.findById(UUID.fromString(uuid)).map(badWordMapper::toDomain);
  }

  @Override
  public Page<BadWord> findAll(Pageable pageable) {
    return badWordRepository.findAll(pageable).map(badWordMapper::toDomain);
  }

  @Override
  public List<BadWord> findAll() {
    return badWordMapper.toDomainList(badWordRepository.findAll());
  }

  @Override
  public BadWord saveBadWord(BadWord badWord) {
    return badWordMapper.toDomain(badWordRepository.save(badWordMapper.toEntity(badWord)));
  }

  @Override
  public BadWord updateBadWord(BadWord badWord) {
    BadWordEntity badWordEntity =
        badWordRepository.findById(UUID.fromString(badWord.getId())).orElseThrow();

    if (badWord.getWordTranslations() != null) {
      badWordEntity.setWordTranslations(
          translationMapper.toEntitySet(badWord.getWordTranslations()));
    }

    return badWordMapper.toDomain(badWordRepository.save(badWordEntity));
  }

  @Override
  public void deleteBadWord(String id) {
    badWordRepository.deleteById(UUID.fromString(id));
  }
}
