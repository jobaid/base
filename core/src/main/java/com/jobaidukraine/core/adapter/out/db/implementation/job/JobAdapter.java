package com.jobaidukraine.core.adapter.out.db.implementation.job;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.*;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobAdapter implements JobPort {

  private final JobRepository jobRepository;
  private final JobMapper jobMapper;
  private final AddressMapper addressMapper;
  private final JobTypeMapper jobTypeMapper;
  private final LanguageSkillMapper languageSkillMapper;
  private final SkillMapper skillMapper;
  private final TranslationMapper translationMapper;

  @Override
  public Optional<Job> findById(String id) {
    return jobRepository.findById(UUID.fromString(id)).map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable) {
    return jobRepository.findAll(pageable).map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByPageableWithBadWords(Pageable pageable) {
    return jobRepository.findAllByBadWordFlag(pageable, true).map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByPageableAndNoBadWords(Pageable pageable) {
    return jobRepository.findAll(pageable).map(jobMapper::toDomain);
  }

  @Override
  public Page<Job>
      findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
          Pageable pageable, String title, String description, String skill) {
    return jobRepository
        .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
            pageable, title, description, skill, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String title, String description) {
    return jobRepository
        .findAllByTitlesValueContainingOrDescriptionsValueContainingAndBadWordFlag(
            pageable, title, description, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String title, String skill) {
    return jobRepository
        .findAllByTitlesValueContainingOrSkillsNameContainingAndBadWordFlag(
            pageable, title, skill, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String description, String skill) {
    return jobRepository
        .findAllByDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
            pageable, description, skill, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingAndNoBadWords(Pageable pageable, String title) {
    return jobRepository
        .findAllByTitlesValueContainingAndBadWordFlag(pageable, title, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String description) {
    return jobRepository
        .findAllByDescriptionsValueContainingAndBadWordFlag(pageable, description, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllBySkillsNameContainingAndNoBadWords(Pageable pageable, String skill) {
    return jobRepository
        .findAllBySkillsNameContainingAndBadWordFlag(pageable, skill, false)
        .map(jobMapper::toDomain);
  }

  @Override
  public Job save(Job job) {
    if (job.getId() == null) {
      job.setId(UUID.randomUUID().toString());
    }
    if (job.getEmployerId() == null) {
      throw new DataIntegrityViolationException("Job must have an employer assigned!");
    }
    return jobMapper.toDomain(jobRepository.save(jobMapper.toEntity(job)));
  }

  @Override
  public Job update(Job job) {
    JobEntity jobEntity = jobRepository.findById(UUID.fromString(job.getId())).orElseThrow();

    if (job.getAddress() != null) {
      jobEntity.setAddress(addressMapper.toEntity(job.getAddress()));
    }

    if (job.getBadWordFlag() != null) {
      jobEntity.setBadWordFlag(job.getBadWordFlag());
    }

    if (job.getTitles() != null) {
      jobEntity.setTitles(translationMapper.toEntitySet(new HashSet<>(job.getTitles())));
    }

    if (job.getDescriptions() != null) {
      jobEntity.setDescriptions(
          translationMapper.toEntitySet(new HashSet<>(job.getDescriptions())));
    }

    if (job.getJobType() != null) {
      jobEntity.setJobType(jobTypeMapper.toEntity(job.getJobType()));
    }

    if (job.getApplicationLink() != null) {
      jobEntity.setApplicationLink(job.getApplicationLink());
    }

    if (job.getApplicationMail() != null) {
      jobEntity.setApplicationMail(job.getApplicationMail());
    }

    if (job.getVideoUrl() != null) {
      jobEntity.setVideoUrl(job.getVideoUrl());
    }

    if (job.getLanguages() != null) {
      jobEntity.setLanguages(languageSkillMapper.toEntitySet(new HashSet<>(job.getLanguages())));
    }

    if (job.getSkills() != null) {
      jobEntity.setSkills(skillMapper.toEntitySet(job.getSkills()));
    }

    return jobMapper.toDomain(jobRepository.save(jobEntity));
  }

  @Override
  public void delete(String id) {
    jobRepository.deleteById(UUID.fromString(id));
  }
}
