package com.jobaidukraine.core.adapter.out.db.implementation.user;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.AddressMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.RoleMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.UserMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserAdapter implements UserPort {

  private final UserRepository<UserEntity> userRepository;
  private final UserMapper userMapper;
  private final RoleMapper roleMapper;
  private final AddressMapper addressMapper;

  @Override
  public Optional<User> findById(String id) {
    return userRepository.findById(UUID.fromString(id)).map(userMapper::toDomain);
  }

  @Override
  public Page<User> findAllByPageable(Pageable pageable) {
    return userRepository.findAll(pageable).map(userMapper::toDomain);
  }

  @Override
  public Optional<User> findByEmail(String email) {
    return userRepository.findByEmail(email).map(userMapper::toDomain);
  }

  @Override
  public User save(User user) {
    return userMapper.toDomain(userRepository.save(userMapper.toEntity(user)));
  }

  @Override
  public User update(User user) {
    UserEntity userEntity = userRepository.findById(UUID.fromString(user.getId())).orElseThrow();

    if (user.getRole() != null) {
      userEntity.setRole(roleMapper.toEntity(user.getRole()));
    }

    if (user.getAddress() != null) {
      userEntity.setAddress(addressMapper.toEntity(user.getAddress()));
    }

    if (user.getEmail() != null) {
      userEntity.setEmail(user.getEmail());
    }

    if (user.getFirstname() != null) {
      userEntity.setFirstname(user.getFirstname());
    }

    if (user.getLastname() != null) {
      userEntity.setLastname(user.getLastname());
    }

    if (user.getDateOfBirth() != null) {
      userEntity.setDateOfBirth(user.getDateOfBirth());
    }

    if (user.getOriginCountry() != null) {
      userEntity.setOriginCountry(user.getOriginCountry());
    }

    return userMapper.toDomain(userRepository.save(userEntity));
  }

  @Override
  public void delete(String id) {
    userRepository.deleteById(UUID.fromString(id));
  }
}
