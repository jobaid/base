package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.AddressEntity;
import com.jobaidukraine.core.domain.values.Address;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityAddressMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {TranslationMapper.class})
public interface AddressMapper {
  Address toDomain(AddressEntity addressEntity);

  AddressEntity toEntity(Address address);
}
