package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntity;
import com.jobaidukraine.core.domain.values.BadWord;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityBadWordMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {TranslationMapper.class},
    imports = {UUID.class})
public interface BadWordMapper {

  @Mapping(
      target = "id",
      expression = "java(badWordEntity.getId() != null ? badWordEntity.getId().toString() : null)")
  BadWord toDomain(BadWordEntity badWordEntity);

  @Mapping(
      target = "id",
      expression = "java(badWord.getId() != null ? UUID.fromString(badWord.getId()) : null)")
  BadWordEntity toEntity(BadWord badWord);

  Set<BadWord> toDomainSet(Set<BadWordEntity> badWordEntities);

  Set<BadWordEntity> toEntity(Set<BadWord> badWords);

  List<BadWord> toDomainList(List<BadWordEntity> badWordEntities);
}
