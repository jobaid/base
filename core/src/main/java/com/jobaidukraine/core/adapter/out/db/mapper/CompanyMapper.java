package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.CompanyEntity;
import com.jobaidukraine.core.domain.values.Company;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntityCompanyMapperImpl")
public interface CompanyMapper {
  Company toDomain(CompanyEntity companyEntity);

  CompanyEntity toEntity(Company company);
}
