package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.EmployerEntity;
import com.jobaidukraine.core.domain.entities.Employer;
import java.util.UUID;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityEmployerMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      AddressMapper.class,
      RoleMapper.class,
      CompanyMapper.class,
      JobMapper.class,
    },
    imports = {UUID.class})
public interface EmployerMapper {
  @Mapping(target = "id", expression = "java(employerEntity.getId().toString())")
  Employer toDomain(EmployerEntity employerEntity);

  @Mapping(target = "id", expression = "java(UUID.fromString(employer.getId()))")
  EmployerEntity toEntity(Employer employer);
}
