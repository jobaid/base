package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.domain.entities.Job;
import java.util.Set;
import java.util.UUID;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityJobMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      LanguageSkillMapper.class,
      AddressMapper.class,
      SkillMapper.class,
      SkillLevelMapper.class,
      JobTypeMapper.class,
      TranslationMapper.class
    },
    imports = {UUID.class})
public interface JobMapper {

  @Mapping(
      target = "id",
      expression = "java(jobEntity.getId() != null ? jobEntity.getId().toString() : null)")
  @Mapping(
      target = "employerId",
      expression =
          "java(jobEntity.getEmployerId() != null ? jobEntity.getEmployerId().toString() : null)")
  Job toDomain(JobEntity jobEntity);

  @Mapping(
      target = "id",
      expression = "java(job.getId() != null ? UUID.fromString(job.getId()) : null)")
  @Mapping(
      target = "employerId",
      expression =
          "java(job.getEmployerId() != null ? UUID.fromString(job.getEmployerId()) : null)")
  JobEntity toEntity(Job job);

  Set<Job> toDomainSet(Set<JobEntity> jobEntities);

  Set<JobEntity> toEntitySet(Set<Job> jobs);
}
