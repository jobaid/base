package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.JobTypeEntity;
import com.jobaidukraine.core.domain.values.JobType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntityJobTypeMapperImpl")
public interface JobTypeMapper {
  JobType toDomain(JobTypeEntity jobTypeEntity);

  JobTypeEntity toEntity(JobType jobType);
}
