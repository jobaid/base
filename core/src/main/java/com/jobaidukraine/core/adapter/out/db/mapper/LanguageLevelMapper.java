package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.domain.values.LanguageLevel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntityLanguageLevelMapperImpl")
public interface LanguageLevelMapper {
  LanguageLevel toDomain(LanguageLevelEntity languageLevelEntity);

  LanguageLevelEntity toEntity(LanguageLevel languageLevel);
}
