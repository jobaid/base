package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.LanguageSkillEntity;
import com.jobaidukraine.core.domain.values.LanguageSkill;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityLanguageSkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {LanguageLevelMapper.class})
public interface LanguageSkillMapper {
  LanguageSkill toDomain(LanguageSkillEntity languageSkillEntity);

  LanguageSkillEntity toEntity(LanguageSkill languageSkill);

  Set<LanguageSkill> toDomainSet(Set<LanguageSkillEntity> languageEntities);

  Set<LanguageSkillEntity> toEntitySet(Set<LanguageSkill> languageLevels);
}
