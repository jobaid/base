package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.RefugeeEntity;
import com.jobaidukraine.core.domain.entities.Refugee;
import java.util.UUID;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityRefugeeMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {
      AddressMapper.class,
      RoleMapper.class,
      LanguageSkillMapper.class,
      SkillMapper.class,
    },
    imports = {UUID.class})
public interface RefugeeMapper {
  @Mapping(target = "id", expression = "java(refugeeEntity.getId().toString())")
  Refugee toDomain(RefugeeEntity refugeeEntity);

  @Mapping(target = "id", expression = "java(UUID.fromString(refugee.getId()))")
  RefugeeEntity toEntity(Refugee refugee);
}
