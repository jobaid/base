package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.RoleEntity;
import com.jobaidukraine.core.domain.values.Role;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntityRoleMapperImpl")
public interface RoleMapper {
  Role toDomain(RoleEntity roleEntity);

  RoleEntity toEntity(Role role);
}
