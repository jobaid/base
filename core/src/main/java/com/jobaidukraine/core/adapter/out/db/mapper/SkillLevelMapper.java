package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.SkillLevelEntity;
import com.jobaidukraine.core.domain.values.SkillLevel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntitySkillLevelMapperImpl")
public interface SkillLevelMapper {
  SkillLevel toDomain(SkillLevelEntity skillLevelEntity);

  SkillLevelEntity toEntity(SkillLevel skillLevel);
}
