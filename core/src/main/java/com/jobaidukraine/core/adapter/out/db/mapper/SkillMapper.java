package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.SkillEntity;
import com.jobaidukraine.core.domain.values.Skill;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "EntitySkillMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {SkillLevelMapper.class})
public interface SkillMapper {
  Skill toDomain(SkillEntity skillEntity);

  SkillEntity toEntity(Skill skill);

  Set<Skill> toDomainSet(Set<SkillEntity> skillEntities);

  Set<SkillEntity> toEntitySet(Set<Skill> skills);
}
