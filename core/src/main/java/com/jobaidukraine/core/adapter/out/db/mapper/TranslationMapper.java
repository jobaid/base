package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.TranslationEntity;
import com.jobaidukraine.core.domain.values.Translation;
import java.util.Set;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityTranslationMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {TranslationTypeMapper.class})
public interface TranslationMapper {
  Translation toDomain(TranslationEntity translationEntity);

  TranslationEntity toEntity(Translation translation);

  Set<Translation> toDomainSet(Set<TranslationEntity> translationEntitySet);

  Set<TranslationEntity> toEntitySet(Set<Translation> translationSet);
}
