package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.TranslationTypeEntity;
import com.jobaidukraine.core.domain.values.TranslationType;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "EntityTranslationTypeMapperImpl")
public interface TranslationTypeMapper {
  TranslationType toDomain(TranslationTypeEntity translationTypeEntity);

  TranslationTypeEntity toDto(TranslationType translationType);
}
