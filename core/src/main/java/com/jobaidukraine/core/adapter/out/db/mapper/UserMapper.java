package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.domain.entities.User;
import java.util.UUID;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "EntityUserMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class, RoleMapper.class},
    imports = {UUID.class})
public interface UserMapper {
  @Mapping(
      target = "id",
      expression = "java(userEntity.getId() != null ? userEntity.getId().toString() : null)")
  User toDomain(UserEntity userEntity);

  @Mapping(
      target = "id",
      expression = "java(user.getId() != null ? UUID.fromString(user.getId()) : null)")
  UserEntity toEntity(User user);
}
