package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadWordRepository extends JpaRepository<BadWordEntity, UUID> {}
