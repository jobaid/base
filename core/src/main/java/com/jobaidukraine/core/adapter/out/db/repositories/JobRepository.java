package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, UUID> {

  Page<JobEntity>
      findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
          Pageable pageable, String title, String description, String skill, Boolean badWordFlag);

  Page<JobEntity> findAllByTitlesValueContainingOrDescriptionsValueContainingAndBadWordFlag(
      Pageable pageable, String title, String description, Boolean badWordFlag);

  Page<JobEntity> findAllByTitlesValueContainingOrSkillsNameContainingAndBadWordFlag(
      Pageable pageable, String title, String skill, Boolean badWordFlag);

  Page<JobEntity> findAllByDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
      Pageable pageable, String description, String skill, Boolean badWordFlag);

  Page<JobEntity> findAllByTitlesValueContainingAndBadWordFlag(
      Pageable pageable, String title, Boolean badWordFlag);

  Page<JobEntity> findAllByDescriptionsValueContainingAndBadWordFlag(
      Pageable pageable, String description, Boolean badWordFlag);

  Page<JobEntity> findAllBySkillsNameContainingAndBadWordFlag(
      Pageable pageable, String skill, Boolean badWordFlag);

  Page<JobEntity> findAllByBadWordFlag(Pageable pageable, Boolean badWordFlag);
}
