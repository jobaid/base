package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository<T extends UserEntity> extends JpaRepository<T, UUID> {

  Optional<T> findByEmail(String email);
}
