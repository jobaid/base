package com.jobaidukraine.core.domain.entities;

import com.jobaidukraine.core.domain.values.Company;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Employer extends User {
  private Company company;
  private Set<Job> jobs;
}
