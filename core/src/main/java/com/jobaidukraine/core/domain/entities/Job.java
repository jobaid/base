package com.jobaidukraine.core.domain.entities;

import com.jobaidukraine.core.domain.values.*;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Job extends Auditable {
  private String id;

  // address
  private Address address;

  // bad-word flag
  private Boolean badWordFlag;

  // jobInformation
  private Set<Translation> titles;
  private Set<Translation> descriptions;
  private JobType jobType;
  private String applicationLink;
  private String applicationMail;
  private String videoUrl;

  // skills
  private Set<LanguageSkill> languages;
  private Set<Skill> skills;

  private String employerId;
}
