package com.jobaidukraine.core.domain.entities;

import com.jobaidukraine.core.domain.values.LanguageSkill;
import com.jobaidukraine.core.domain.values.Skill;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Refugee extends User {
  private Set<LanguageSkill> languages;
  private Set<Skill> skills;
}
