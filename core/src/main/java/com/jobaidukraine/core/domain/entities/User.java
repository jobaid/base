package com.jobaidukraine.core.domain.entities;

import com.jobaidukraine.core.domain.values.Address;
import com.jobaidukraine.core.domain.values.Auditable;
import com.jobaidukraine.core.domain.values.Role;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class User extends Auditable {
  private String id;

  // access_level
  private Role role;

  // address
  private Address address;

  // personals
  private String email;
  private String firstname;
  private String lastname;
  private LocalDate dateOfBirth;
  private String originCountry;
}
