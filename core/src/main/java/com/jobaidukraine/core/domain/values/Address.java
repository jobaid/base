package com.jobaidukraine.core.domain.values;

import java.util.Set;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Address {
  Float latitude;
  Float longitude;
  String street;
  String houseNumber;
  String zip;
  Set<Translation> cityTranslations;
  String country;
}
