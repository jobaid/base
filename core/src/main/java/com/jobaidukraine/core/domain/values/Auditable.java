package com.jobaidukraine.core.domain.values;

import java.time.LocalDateTime;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
public abstract class Auditable {

  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
}
