package com.jobaidukraine.core.domain.values;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BadWord {
  private String id;
  private Set<Translation> wordTranslations;
}
