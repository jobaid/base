package com.jobaidukraine.core.domain.values;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Company {
  String name;
  String url;
  String email;
  String logo;
}
