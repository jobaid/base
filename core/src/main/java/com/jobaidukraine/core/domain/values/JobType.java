package com.jobaidukraine.core.domain.values;

public enum JobType {
  FREELANCE("FREELANCE"),
  FULL_TIME("FULL_TIME"),
  PART_TIME("PART_TIME"),
  TEMPORARY("TEMPORARY");

  final String type;

  JobType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
