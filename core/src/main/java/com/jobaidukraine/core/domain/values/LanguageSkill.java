package com.jobaidukraine.core.domain.values;

import java.util.Locale;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class LanguageSkill {
  Locale locale;
  LanguageLevel level;
}
