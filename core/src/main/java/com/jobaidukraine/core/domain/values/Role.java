package com.jobaidukraine.core.domain.values;

public enum Role {
  ADMIN("ROLE_ADMIN"),
  MODERATOR("ROLE_MODERATOR"),
  REFUGEE("ROLE_REFUGEE"),
  EMPLOYER("ROLE_EMPLOYER");

  final String roleName;

  Role(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleName() {
    return roleName;
  }
}
