package com.jobaidukraine.core.domain.values;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Skill {
  String name;
  SkillLevel level;
}
