package com.jobaidukraine.core.domain.values;

public enum SkillLevel {
  BEGINNER("BEGINNER"),
  INTERMEDIATE("INTERMEDIATE"),
  PROFESSIONAL("PROFESSIONAL");

  final String skill;

  SkillLevel(String skill) {
    this.skill = skill;
  }

  public String getSkill() {
    return this.skill;
  }
}
