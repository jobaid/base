package com.jobaidukraine.core.domain.values;

import java.time.LocalDateTime;
import java.util.Locale;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Translation {
  Locale locale;

  String value;

  TranslationType type;

  LocalDateTime translatedOn;

  String translatedBy;
}
