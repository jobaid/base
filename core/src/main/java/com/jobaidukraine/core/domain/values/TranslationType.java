package com.jobaidukraine.core.domain.values;

public enum TranslationType {
  AUTO_TRANSLATION("AUTO_TRANSLATION"),
  NATIVE_TRANSLATION("NATIVE_TRANSLATION"),
  ORIGINAL_TRANSLATION("ORIGINAL_TRANSLATION");

  final String type;

  private TranslationType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
