package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.values.BadWord;
import com.jobaidukraine.core.domain.values.Translation;
import com.jobaidukraine.core.services.ports.in.BadWordUseCase;
import com.jobaidukraine.core.services.ports.out.BadWordPort;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BadWordService implements BadWordUseCase {

  private final BadWordPort badWordPort;

  @Override
  public Boolean checkTranslationForBadWords(Translation translation) {
    List<BadWord> badWords = badWordPort.findAll();

    for (BadWord badWord : badWords) {
      for (Translation wordTranslation : badWord.getWordTranslations()) {
        if (translation.getValue().contains(wordTranslation.getValue())) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public BadWord findById(String uuid) {
    return badWordPort.findById(uuid).orElseThrow();
  }

  @Override
  public Page<BadWord> findAll(Pageable pageable) {
    return badWordPort.findAll(pageable);
  }

  @Override
  public BadWord saveBadWord(BadWord badWord) {
    return badWordPort.saveBadWord(badWord);
  }

  @Override
  public BadWord updateBadWord(BadWord badWord) {
    return badWordPort.updateBadWord(badWord);
  }

  @Override
  public void deleteBadWord(String id) {
    badWordPort.deleteBadWord(id);
  }
}
