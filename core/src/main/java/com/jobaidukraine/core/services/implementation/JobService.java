package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.services.ports.in.BadWordUseCase;
import com.jobaidukraine.core.services.ports.in.JobUseCase;
import com.jobaidukraine.core.services.ports.out.JobPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobService implements JobUseCase {

  private final JobPort jobPort;
  private final BadWordUseCase badWordUseCase;

  @Override
  public Job save(Job job, String employerId) {
    if (job.getTitles().parallelStream().noneMatch(badWordUseCase::checkTranslationForBadWords)) {
      job.setBadWordFlag(true);
    }

    if (job.getDescriptions().parallelStream()
        .noneMatch(badWordUseCase::checkTranslationForBadWords)) {
      job.setBadWordFlag(true);
    }

    job.setEmployerId(employerId);
    return jobPort.save(job);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable) {
    return this.jobPort.findAllByPageable(pageable);
  }

  @Override
  public Page<Job> findAllByPageableWithBadWords(Pageable pageable) {
    return jobPort.findAllByPageableWithBadWords(pageable);
  }

  @Override
  public Page<Job> findAllByPageableAndNoBadWords(Pageable pageable) {
    return this.jobPort.findAllByPageableAndNoBadWords(pageable);
  }

  @Override
  public Page<Job>
      findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
          Pageable pageable, String title, String description, String skill) {
    return this.jobPort
        .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            pageable, title, description, skill);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String title, String description) {
    return this.jobPort.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
        pageable, title, description);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String title, String skill) {
    return this.jobPort.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
        pageable, title, skill);
  }

  @Override
  public Page<Job> findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String description, String skill) {
    return this.jobPort.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
        pageable, description, skill);
  }

  @Override
  public Page<Job> findAllByTitlesValueContainingAndNoBadWords(Pageable pageable, String title) {
    return this.jobPort.findAllByTitlesValueContainingAndNoBadWords(pageable, title);
  }

  @Override
  public Page<Job> findAllByDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String description) {
    return this.jobPort.findAllByDescriptionsValueContainingAndNoBadWords(pageable, description);
  }

  @Override
  public Page<Job> findAllBySkillsNameContainingAndNoBadWords(Pageable pageable, String skill) {
    return this.jobPort.findAllBySkillsNameContainingAndNoBadWords(pageable, skill);
  }

  @Override
  public Job findById(String id) {
    return this.jobPort.findById(id).orElseThrow();
  }

  @Override
  public Job update(Job job) {
    if (job.getTitles() != null
        && job.getTitles().parallelStream()
            .noneMatch(badWordUseCase::checkTranslationForBadWords)) {
      job.setBadWordFlag(true);
    }

    if (job.getDescriptions() != null
        && job.getDescriptions().parallelStream()
            .noneMatch(badWordUseCase::checkTranslationForBadWords)) {
      job.setBadWordFlag(true);
    }

    return this.jobPort.update(job);
  }

  @Override
  public void delete(String id) {
    jobPort.delete(id);
  }
}
