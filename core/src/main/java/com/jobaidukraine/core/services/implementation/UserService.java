package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.services.ports.in.UserUseCase;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements UserUseCase {

  private final UserPort userPort;

  @Override
  public User save(User user) {
    return this.userPort.save(user);
  }

  @Override
  public Page<User> findAllByPageable(Pageable pageable) {
    return this.userPort.findAllByPageable(pageable);
  }

  @Override
  public User findById(String id) {
    return this.userPort.findById(id).orElseThrow();
  }

  @Override
  public User update(User user) {
    return userPort.update(user);
  }

  @Override
  public void delete(String id) {
    this.userPort.delete(id);
  }

  @Override
  public Optional<User> findByEmail(String email) {
    return this.userPort.findByEmail(email);
  }
}
