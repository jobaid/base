package com.jobaidukraine.core.services.ports.in;

import com.jobaidukraine.core.domain.values.BadWord;
import com.jobaidukraine.core.domain.values.Translation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BadWordUseCase {

  Boolean checkTranslationForBadWords(Translation translation);

  BadWord findById(String uuid);

  Page<BadWord> findAll(Pageable pageable);

  BadWord saveBadWord(BadWord badWord);

  BadWord updateBadWord(BadWord badWord);

  void deleteBadWord(String id);
}
