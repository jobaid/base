package com.jobaidukraine.core.services.ports.in;

import com.jobaidukraine.core.domain.entities.Employer;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployerUseCase {

  Employer update(Employer employer);

  void delete(long id);

  Employer findById(long id);

  Page<Employer> findAllByPageable(Pageable pageable);

  Optional<Employer> findByEmail(String email);
}
