package com.jobaidukraine.core.services.ports.in;

import com.jobaidukraine.core.domain.entities.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JobUseCase {
  Job save(Job job, String employerId);

  Job update(Job job);

  void delete(String id);

  Job findById(String id);

  Page<Job> findAllByPageable(Pageable pageable);

  Page<Job> findAllByPageableWithBadWords(Pageable pageable);

  Page<Job> findAllByPageableAndNoBadWords(Pageable pageable);

  Page<Job>
      findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
          Pageable pageable, String title, String description, String skill);

  Page<Job> findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String title, String description);

  Page<Job> findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String title, String skill);

  Page<Job> findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String description, String skill);

  Page<Job> findAllByTitlesValueContainingAndNoBadWords(Pageable pageable, String title);

  Page<Job> findAllByDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String description);

  Page<Job> findAllBySkillsNameContainingAndNoBadWords(Pageable pageable, String skill);
}
