package com.jobaidukraine.core.services.ports.in;

import com.jobaidukraine.core.domain.entities.Refugee;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RefugeeUseCase {
  Refugee save(Refugee refugee);

  Refugee update(Refugee refugee);

  void delete(long id);

  Refugee findById(long id);

  Page<Refugee> findAllByPageable(Pageable pageable);

  Optional<Refugee> findByEmail(String email);
}
