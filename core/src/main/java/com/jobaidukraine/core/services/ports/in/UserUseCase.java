package com.jobaidukraine.core.services.ports.in;

import com.jobaidukraine.core.domain.entities.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserUseCase {
  User save(User user);

  User update(User user);

  void delete(String id);

  User findById(String id);

  Page<User> findAllByPageable(Pageable pageable);

  Optional<User> findByEmail(String email);
}
