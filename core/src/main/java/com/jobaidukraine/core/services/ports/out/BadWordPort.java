package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.values.BadWord;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BadWordPort {
  Optional<BadWord> findById(String uuid);

  Page<BadWord> findAll(Pageable pageable);

  List<BadWord> findAll();

  BadWord saveBadWord(BadWord badWord);

  BadWord updateBadWord(BadWord badWord);

  void deleteBadWord(String id);
}
