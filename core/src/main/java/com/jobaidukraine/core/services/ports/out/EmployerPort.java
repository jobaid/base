package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.entities.Employer;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployerPort {
  Optional<Employer> findById(String id);

  Page<Employer> findAllByPageable(Pageable pageable);

  Optional<Employer> findByEmail(String email);

  Employer save(Employer employer);

  Employer update(Employer employer);

  void delete(String id);
}
