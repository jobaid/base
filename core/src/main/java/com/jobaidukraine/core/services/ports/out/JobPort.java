package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.entities.Job;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JobPort {
  Optional<Job> findById(String id);

  Page<Job> findAllByPageable(Pageable pageable);

  Page<Job> findAllByPageableWithBadWords(Pageable pageable);

  Page<Job> findAllByPageableAndNoBadWords(Pageable pageable);

  Page<Job>
      findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
          Pageable pageable, String title, String description, String skill);

  Page<Job> findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String title, String description);

  Page<Job> findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String title, String skill);

  Page<Job> findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
      Pageable pageable, String description, String skill);

  Page<Job> findAllByTitlesValueContainingAndNoBadWords(Pageable pageable, String title);

  Page<Job> findAllByDescriptionsValueContainingAndNoBadWords(
      Pageable pageable, String description);

  Page<Job> findAllBySkillsNameContainingAndNoBadWords(Pageable pageable, String skill);

  Job save(Job job);

  Job update(Job job);

  void delete(String id);
}
