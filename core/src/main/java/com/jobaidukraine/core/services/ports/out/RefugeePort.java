package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.entities.Refugee;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RefugeePort {
  Optional<Refugee> findById(String id);

  Page<Refugee> findAllByPageable(Pageable pageable);

  Optional<Refugee> findByEmail(String email);

  Refugee save(Refugee refugee);

  Refugee update(Refugee refugee);

  void delete(String id);
}
