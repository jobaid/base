package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDto;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDtoMother;
import com.jobaidukraine.core.adapter.in.rest.dto.TranslationDtoMother;
import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntity;
import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntityMother;
import com.jobaidukraine.core.adapter.out.db.entities.EmployerEntity;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntityMother;
import com.jobaidukraine.core.adapter.out.db.repositories.BadWordRepository;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@AutoConfigureMockMvc
@Tag("integration")
@ActiveProfiles("local")
@Testcontainers
public class BadWordControllerIntegrationTest {

  @Autowired private MockMvc mock;
  @Autowired private UserRepository<EmployerEntity> userRepository;
  @Autowired private BadWordRepository badWordRepository;

  @Container
  private static final MariaDBContainer<?> database = new MariaDBContainer<>("mariadb:latest");

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", database::getJdbcUrl);
    registry.add("spring.datasource.username", database::getUsername);
    registry.add("spring.datasource.password", database::getPassword);
  }

  static final String BAD_WORD_BASE_URL = "/v1.0/bad-words/";
  private static final String EMPLOYER_ID = "00000000-0000-0000-0000-000000000000";

  @BeforeEach
  public void init() {
    EmployerEntity employer =
        (EmployerEntity)
            UserEntityMother.completeEmployer()
                .jobs(Collections.emptySet())
                .id(UUID.fromString(EMPLOYER_ID))
                .build();
    userRepository.save(employer);
  }

  @AfterEach
  public void cleanup() {
    badWordRepository.deleteAll();
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void save() throws Exception {
    BadWordDto badWordDto = BadWordDtoMother.germanBadWord().build();

    mock.perform(
            post(BAD_WORD_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getBadWordJson(badWordDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.wordTranslations[0].value").value("Schimpfwort"));
  }

  @Test
  void list() throws Exception {
    badWordRepository.saveAll(
        List.of(
            BadWordEntityMother.germanBadWord().build(),
            BadWordEntityMother.englishBadWord().build()));

    mock.perform(get(BAD_WORD_BASE_URL).param("page", "0").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(
            jsonPath(
                "$._embedded.badWordDtoes[*].wordTranslations[0].value",
                Matchers.containsInAnyOrder("Swear word", "Schimpfwort")));
  }

  @Test
  void show() throws Exception {
    BadWordEntity badWordEntity =
        badWordRepository.save(BadWordEntityMother.germanBadWord().build());

    mock.perform(get(BAD_WORD_BASE_URL + "/" + badWordEntity.getId()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.wordTranslations[0].value").value("Schimpfwort"));
  }

  @Test
  void update() throws Exception {
    BadWordEntity badWordEntity =
        badWordRepository.save(BadWordEntityMother.germanBadWord().build());
    BadWordDto badWordDto =
        BadWordDtoMother.germanBadWord()
            .id(badWordEntity.getId().toString())
            .wordTranslations(
                Set.of(
                    TranslationDtoMother.germanTranslation().value("Schimpfwort").build(),
                    TranslationDtoMother.englishTranslation().value("Swear word").build()))
            .build();

    mock.perform(
            patch(BAD_WORD_BASE_URL + "/" + badWordEntity.getId())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getBadWordJson(badWordDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(badWordDto.getId()))
        .andExpect(
            jsonPath(
                "$.wordTranslations[*].value",
                Matchers.containsInAnyOrder("Schimpfwort", "Swear word")));
  }

  @Test
  void deleteBadWord() throws Exception {
    BadWordEntity badWord = badWordRepository.save(BadWordEntityMother.germanBadWord().build());

    mock.perform(delete(BAD_WORD_BASE_URL + "/{id}", badWord.getId())).andExpect(status().isOk());

    mock.perform(get(BAD_WORD_BASE_URL).param("page", "0").param("size", "2"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded").doesNotExist());
  }

  String getBadWordJson(BadWordDto badWordDto) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(badWordDto);
  }
}
