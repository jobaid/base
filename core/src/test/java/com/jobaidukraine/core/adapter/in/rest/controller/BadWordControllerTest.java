package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.BadWordResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDto;
import com.jobaidukraine.core.adapter.in.rest.dto.BadWordDtoMother;
import com.jobaidukraine.core.adapter.in.rest.mapper.BadWordMapper;
import com.jobaidukraine.core.domain.BadWordMother;
import com.jobaidukraine.core.domain.values.BadWord;
import com.jobaidukraine.core.services.ports.in.BadWordUseCase;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(BadWordController.class)
@WithMockUser(
    username = "Picard",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class BadWordControllerTest {

  @Autowired private MockMvc mock;
  @MockBean private BadWordUseCase badWordUseCase;
  @MockBean private PagedResourcesAssembler<BadWordDto> pagedResourcesAssembler;
  @MockBean private BadWordResourceAssembler badWordResourceAssembler;
  @MockBean private BadWordMapper badWordMapper;

  static final String BAD_WORD_BASE_URL = "/v1.0/bad-words";
  private static final String EMPLOYER_ID = "00000000-0000-0000-0000-000000000000";

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void save() throws Exception {
    BadWordDto badWordDto = BadWordDtoMother.germanBadWord().build();
    BadWord badWord = BadWordMother.germanBadWord().build();

    when(badWordMapper.toDomain(any(BadWordDto.class))).thenReturn(badWord);
    when(badWordUseCase.saveBadWord(any(BadWord.class))).thenReturn(badWord);
    when(badWordMapper.toDto(any(BadWord.class))).thenReturn(badWordDto);
    when(badWordResourceAssembler.toModel(any(BadWordDto.class)))
        .thenReturn(EntityModel.of(badWordDto));

    mock.perform(
            post(BAD_WORD_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getBadWordJson(badWordDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(badWordDto.getId()))
        .andExpect(jsonPath("$.wordTranslations[0].value").value("Schimpfwort"));
  }

  @Test
  void list() throws Exception {
    List<BadWord> badWords =
        List.of(
            BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build(),
            BadWordMother.englishBadWord().id("00000000-0000-0000-0000-000000000001").build());

    List<BadWordDto> badWordDtos =
        List.of(
            BadWordDtoMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build(),
            BadWordDtoMother.englishBadWord().id("00000000-0000-0000-0000-000000000001").build());

    Page<BadWord> badWordPage = new PageImpl<>(badWords);
    PagedModel<EntityModel<BadWordDto>> pagedModel =
        PagedModel.wrap(badWordDtos, new PagedModel.PageMetadata(2, 2, 2, 2));

    when(badWordUseCase.findAll(any(Pageable.class))).thenReturn(badWordPage);
    when(badWordMapper.toDto(badWords.get(0))).thenReturn(badWordDtos.get(0));
    when(badWordMapper.toDto(badWords.get(1))).thenReturn(badWordDtos.get(1));
    when(pagedResourcesAssembler.toModel(any(Page.class), any(BadWordResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(BAD_WORD_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(
            jsonPath(
                "$._embedded.badWordDtoes[*].wordTranslations[0].value",
                Matchers.containsInAnyOrder("Schimpfwort", "Swear word")));
  }

  @Test
  void show() throws Exception {
    BadWordDto badWordDto =
        BadWordDtoMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();
    when(badWordUseCase.findById(badWordDto.getId())).thenReturn(badWord);
    when(badWordMapper.toDto(badWord)).thenReturn(badWordDto);
    when(badWordResourceAssembler.toModel(any(BadWordDto.class)))
        .thenReturn(EntityModel.of(badWordDto));

    mock.perform(get(BAD_WORD_BASE_URL + "/" + badWordDto.getId()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.wordTranslations[0].value").value("Schimpfwort"));
  }

  @Test
  void update() throws Exception {
    BadWordDto badWordDto = BadWordDtoMother.germanBadWord().build();
    BadWord badWord = BadWordMother.germanBadWord().build();

    when(badWordMapper.toDomain(any(BadWordDto.class))).thenReturn(badWord);
    when(badWordUseCase.updateBadWord(any(BadWord.class))).thenReturn(badWord);
    when(badWordMapper.toDto(any(BadWord.class))).thenReturn(badWordDto);
    when(badWordResourceAssembler.toModel(any(BadWordDto.class)))
        .thenReturn(EntityModel.of(badWordDto));

    mock.perform(
            patch(BAD_WORD_BASE_URL + "/{id}", "00000000-0000-0000-0000-000000000000")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getBadWordJson(badWordDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(badWordDto.getId()))
        .andExpect(jsonPath("$.wordTranslations[0].value").value("Schimpfwort"));
  }

  @Test
  void deleteBadWord() throws Exception {
    mock.perform(delete(BAD_WORD_BASE_URL + "/{id}", "00000000-0000-0000-0000-000000000000"))
        .andExpect(status().isOk());
    verify(badWordUseCase).deleteBadWord("00000000-0000-0000-0000-000000000000");
  }

  String getBadWordJson(BadWordDto badWordDto) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(badWordDto);
  }
}
