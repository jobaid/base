package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.dto.*;
import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntityMother;
import com.jobaidukraine.core.adapter.out.db.entities.EmployerEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntityMother;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntityMother;
import com.jobaidukraine.core.adapter.out.db.repositories.BadWordRepository;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@AutoConfigureMockMvc
@Tag("integration")
@ActiveProfiles("local")
@Testcontainers
public class JobControllerIntegrationTest {

  @Autowired MockMvc mock;
  @Autowired ObjectMapper objectMapper;
  @Autowired JobRepository jobRepository;
  @Autowired UserRepository<EmployerEntity> userRepository;
  @Autowired BadWordRepository badWordRepository;

  @Container
  private static final MariaDBContainer<?> database = new MariaDBContainer<>("mariadb:latest");

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", database::getJdbcUrl);
    registry.add("spring.datasource.username", database::getUsername);
    registry.add("spring.datasource.password", database::getPassword);
  }

  static final String JOB_BASE_URL = "/v1.0/jobs/";
  private static final String EMPLOYER_ID = "c4abd5a6-d228-4a5d-b460-32c074cfa17e";

  @BeforeEach
  public void init() {
    EmployerEntity employer =
        (EmployerEntity)
            UserEntityMother.completeEmployer()
                .jobs(Collections.emptySet())
                .id(UUID.fromString(EMPLOYER_ID))
                .build();
    userRepository.save(employer);
  }

  @AfterEach
  public void cleanup() {
    jobRepository.deleteAll();
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("input is correct, Job gets saved, response 200")
  void addJobIntegrationTest1() throws Exception {
    // when
    String json = objectMapper.writeValueAsString(JobDtoMother.complete().id(null).build());

    // given
    var result =
        mock.perform(post(JOB_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.badWordFlag").value("false"))
            .andReturn()
            .getResponse();

    // then
    mock.perform(
            get(
                JOB_BASE_URL
                    + objectMapper.readValue(result.getContentAsString(), JobDto.class).getId()))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void addJobIntegrationTestWithBadWordInSameLanguage() throws Exception {
    badWordRepository.save(BadWordEntityMother.germanBadWord().build());

    // when
    String json =
        objectMapper.writeValueAsString(
            JobDtoMother.complete()
                .id(null)
                .titles(
                    Set.of(TranslationDtoMother.germanTranslation().value("Schimpfwort").build()))
                .build());

    // given
    var result =
        mock.perform(post(JOB_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.badWordFlag").value("true"))
            .andReturn()
            .getResponse();

    // then
    mock.perform(
            get(
                JOB_BASE_URL
                    + objectMapper.readValue(result.getContentAsString(), JobDto.class).getId()))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void addJobIntegrationTestWithBadWordInDifferentLanguage() throws Exception {
    badWordRepository.save(BadWordEntityMother.englishBadWord().build());

    // when
    String json =
        objectMapper.writeValueAsString(
            JobDtoMother.complete()
                .id(null)
                .titles(
                    Set.of(TranslationDtoMother.germanTranslation().value("Swear word").build()))
                .build());

    // given
    var result =
        mock.perform(post(JOB_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.badWordFlag").value("true"))
            .andReturn()
            .getResponse();

    // then
    mock.perform(
            get(
                JOB_BASE_URL
                    + objectMapper.readValue(result.getContentAsString(), JobDto.class).getId()))
        .andExpect(status().isOk());
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("input is incorrect, Job does not get saved, response 400")
  void addJobIntegrationTest2() throws Exception {

    String json =
        objectMapper.writeValueAsString(
            JobDtoMother.complete()
                .badWordFlag(null)
                .address(AddressDtoMother.germanAddress().latitude(-100f).build())
                .id(null)
                .build());

    mock.perform(post(JOB_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
        .andExpect(status().is(400));
  }

  @Test
  @DisplayName("list all jobs if there are jobs, response 200")
  void listAllJobsIntegrationTest1() throws Exception {
    for (int i = 0; i < 5; i++) {
      jobRepository.save(JobEntityMother.complete().build());
    }

    mock.perform(get(JOB_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes", hasSize(5)));
  }

  @Test
  void listAllJobsIntegrationTestWithBadWords() throws Exception {
    for (int i = 0; i < 5; i++) {
      jobRepository.save(JobEntityMother.complete().badWordFlag(i % 2 == 0).build());
    }

    mock.perform(get(JOB_BASE_URL + "/moderation").contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes", hasSize(3)));
  }

  @Test
  void showJobIntegrationsTest() throws Exception {
    JobEntity jobEntity = jobRepository.save(JobEntityMother.complete().build());

    mock.perform(
            get(JOB_BASE_URL + jobEntity.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.videoUrl").value(jobEntity.getVideoUrl()));
  }

  @Test
  @DisplayName("try fo find non existing job, with invalid id format, response 400")
  void showJobIntegrationsTest2() throws Exception {
    mock.perform(get(JOB_BASE_URL + "1234").contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(400));
  }

  @Test
  @DisplayName("try fo find non existing job, response 404")
  void showJobIntegrationsTest3() throws Exception {
    mock.perform(
            get(JOB_BASE_URL + UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(404));
  }

  @Test
  @Transactional
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("try to update job with, valid input, repsonse 200")
  void updateJobIntegrationTest1() throws Exception {
    // when
    var jobEntity = jobRepository.save(JobEntityMother.complete().build());

    // given
    var toUpdate =
        JobDtoMother.complete()
            .id(jobEntity.getId().toString())
            .badWordFlag(true)
            .titles(Set.of(TranslationDtoMother.germanTranslation().value("Job").build()))
            .applicationLink("https://apply.jobaidukraine.com")
            .applicationMail("jobs@jobaidukraine.com")
            .videoUrl("https://www.youtube.com/watch?v=X_8Nh5XfRw0")
            .languages(
                Set.of(
                    LanguageSkillDtoMother.ukraineNative().build(),
                    LanguageSkillDtoMother.germanFluent().build()))
            .build();
    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(JOB_BASE_URL + jobEntity.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().isOk());

    var updatedJob = jobRepository.findById(jobEntity.getId()).get();

    // then
    assertThat(updatedJob.getBadWordFlag()).isTrue();
    assertThat(updatedJob.getTitles()).hasSize(1);
    assertThat(updatedJob.getApplicationLink()).isEqualTo("https://apply.jobaidukraine.com");
    assertThat(updatedJob.getApplicationMail()).isEqualTo("jobs@jobaidukraine.com");
    assertThat(updatedJob.getVideoUrl()).isEqualTo("https://www.youtube.com/watch?v=X_8Nh5XfRw0");
    assertThat(updatedJob.getLanguages()).hasSize(2);
  }

  @Test
  @Transactional
  @WithMockUserId(userId = EMPLOYER_ID)
  void updateJobIntegrationTestSetBadWord() throws Exception {
    badWordRepository.save(BadWordEntityMother.germanBadWord().build());

    // when
    var jobEntity = jobRepository.save(JobEntityMother.complete().build());

    // given
    var toUpdate =
        JobDtoMother.complete()
            .id(jobEntity.getId().toString())
            .badWordFlag(false)
            .titles(Set.of(TranslationDtoMother.germanTranslation().value("Schimpfwort").build()))
            .applicationLink("https://apply.jobaidukraine.com")
            .applicationMail("jobs@jobaidukraine.com")
            .videoUrl("https://www.youtube.com/watch?v=X_8Nh5XfRw0")
            .languages(
                Set.of(
                    LanguageSkillDtoMother.ukraineNative().build(),
                    LanguageSkillDtoMother.germanFluent().build()))
            .build();
    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(JOB_BASE_URL + jobEntity.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().isOk());

    var updatedJob = jobRepository.findById(jobEntity.getId()).get();

    // then
    assertThat(updatedJob.getBadWordFlag()).isTrue();
    assertThat(updatedJob.getTitles()).hasSize(1);
    assertThat(updatedJob.getApplicationLink()).isEqualTo("https://apply.jobaidukraine.com");
    assertThat(updatedJob.getApplicationMail()).isEqualTo("jobs@jobaidukraine.com");
    assertThat(updatedJob.getVideoUrl()).isEqualTo("https://www.youtube.com/watch?v=X_8Nh5XfRw0");
    assertThat(updatedJob.getLanguages()).hasSize(2);
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("try to update job with, invalid input, repsonse 400")
  void updateJobIntegrationTest2() throws Exception {
    // when
    JobEntity jobEntity = jobRepository.save(JobEntityMother.complete().build());

    // given
    JobDto toUpdate =
        JobDtoMother.complete()
            .id(jobEntity.getId().toString())
            .badWordFlag(null)
            .address(AddressDtoMother.germanAddress().latitude(-100f).build())
            .build();
    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(JOB_BASE_URL + jobEntity.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().is(400));
  }

  @Test
  @Transactional
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("try to update job that doesn't exist, repsonse 404")
  void updateJobIntegrationTest3() throws Exception {
    JobDto toUpdate =
        JobDtoMother.complete()
            .id(UUID.randomUUID().toString())
            .badWordFlag(true)
            .titles(Set.of(TranslationDtoMother.germanTranslation().value("Job").build()))
            .applicationLink("https://apply.jobaidukraine.com")
            .applicationMail("jobs@jobaidukraine.com")
            .videoUrl("https://www.youtube.com/watch?v=X_8Nh5XfRw0")
            .languages(
                Set.of(
                    LanguageSkillDtoMother.ukraineNative().build(),
                    LanguageSkillDtoMother.germanFluent().build()))
            .build();
    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(JOB_BASE_URL + toUpdate.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().is(404));
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("delete existing job, response 204")
  void deleteJobIntegrationTest1() throws Exception {
    JobEntity job1 = JobEntityMother.complete().build();
    JobEntity job2 = jobRepository.save(job1);

    mock.perform(delete(JOB_BASE_URL + job2.getId().toString())).andExpect(status().is(204));
    ;
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  @DisplayName("try to delete non existing job, response 404")
  void deleteJobIntegrationTest2() throws Exception {
    mock.perform(delete(JOB_BASE_URL + UUID.randomUUID().toString())).andExpect(status().is(404));
  }
}
