package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.JobResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDtoMother;
import com.jobaidukraine.core.adapter.in.rest.mapper.JobMapper;
import com.jobaidukraine.core.domain.JobMother;
import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.services.ports.in.JobUseCase;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(JobController.class)
@WithMockUser(
    username = "Picard",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class JobControllerTest {

  @Autowired MockMvc mock;
  @MockBean JobUseCase jobUseCase;
  @MockBean PagedResourcesAssembler<JobDto> pagedResourcesAssembler;
  @MockBean JobResourceAssembler jobResourceAssembler;
  @MockBean JobMapper jobMapper;

  static final String JOB_BASE_URL = "/v1.0/jobs";
  static final DateTimeFormatter TIMESTAMP_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
  private static final String EMPLOYER_ID = "c4abd5a6-d228-4a5d-b460-32c074cfa17e";

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void createJobFromJson() throws Exception {
    Job job = JobMother.complete().build();
    JobDto jobDto = JobDtoMother.complete().build();

    when(jobMapper.toDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.save(any(Job.class), any(String.class))).thenReturn(job);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(EntityModel.of(jobDto));

    mock.perform(
            post(JOB_BASE_URL + "?companyId={id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(jobDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(jobDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(jobDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(jobDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(jobDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(jobDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(jobDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.badWordFlag").value(jobDto.getBadWordFlag()))
        .andExpect(jsonPath("$.titles").isArray())
        .andExpect(jsonPath("$.descriptions").isArray())
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType().toString()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.languages").isArray())
        .andExpect(jsonPath("$.skills").isArray());
  }

  @Test
  @WithMockUserId(userId = EMPLOYER_ID)
  void createJobFromJsonInvalidJob() throws Exception {
    Job job = Job.builder().build();
    JobDto jobDto = JobDto.builder().build();

    when(jobMapper.toDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.save(any(Job.class), any(String.class)))
        .thenThrow(new IllegalArgumentException());
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(EntityModel.of(jobDto));

    mock.perform(
            post(JOB_BASE_URL + "?companyId={id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(jobDto)))
        .andExpect(status().isBadRequest());
  }

  @Test
  void readPageableListOfJobs() throws Exception {
    Job job = JobMother.complete().build();
    Job job2 = JobMother.complete().build();
    job2.setId("2");
    Page<Job> jobPage = new PageImpl<>(List.of(job, job2));
    JobDto jobDto = JobDtoMother.complete().build();
    JobDto jobDto2 = JobDtoMother.complete().build();
    jobDto2.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto, jobDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByPageableAndNoBadWords(any(Pageable.class))).thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()))
        .andExpect(jsonPath("$._embedded.jobDtoes[1].id").value(jobDto2.getId()));
  }

  @Test
  void readPageableListOfJobsWithBadWords() throws Exception {
    Job job = JobMother.complete().badWordFlag(true).build();
    Job job2 = JobMother.complete().badWordFlag(true).build();
    job2.setId("2");
    Page<Job> jobPage = new PageImpl<>(List.of(job, job2));
    JobDto jobDto = JobDtoMother.complete().badWordFlag(true).build();
    JobDto jobDto2 = JobDtoMother.complete().badWordFlag(true).build();
    jobDto2.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto, jobDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByPageableWithBadWords(any(Pageable.class))).thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "/moderation"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].badWordFlag").value("true"))
        .andExpect(jsonPath("$._embedded.jobDtoes[1].id").value(jobDto2.getId()))
        .andExpect(jsonPath("$._embedded.jobDtoes[1].badWordFlag").value("true"));
  }

  @Test
  void readPageableListOfJobsWithBadWordsNoJobs() throws Exception {
    Page<Job> jobPage = new PageImpl<>(List.of());
    JobDto jobDto = JobDto.builder().build();
    PagedModel<EntityModel<JobDto>> pagedModel = null;

    when(jobUseCase.findAllByPageableWithBadWords(any(Pageable.class)))
        .thenThrow(new NoSuchElementException());
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "/moderation")).andExpect(status().isNotFound());
  }

  @Test
  void readPageableListOfJobsWithBadWordsInvalidPaqgable() throws Exception {
    Page<Job> jobPage = new PageImpl<>(List.of());
    JobDto jobDto = JobDto.builder().build();
    PagedModel<EntityModel<JobDto>> pagedModel = null;

    when(jobUseCase.findAllByPageableWithBadWords(any(Pageable.class)))
        .thenThrow(new IllegalArgumentException());
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "/moderation")).andExpect(status().isBadRequest());
  }

  @Test
  void readPageableListOfJobsWithTitle() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByTitlesValueContainingAndNoBadWords(
            any(Pageable.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?title={title}", "Test"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithDescription() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByDescriptionsValueContainingAndNoBadWords(
            any(Pageable.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?description={description}", "Test"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithSkill() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllBySkillsNameContainingAndNoBadWords(
            any(Pageable.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?skill={skill}", "Spring Boot"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithDescriptionSkill() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            any(Pageable.class), any(String.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(
            get(JOB_BASE_URL + "?skill={skill}&description={description}", "Spring Boot", "Test"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithTitleSkill() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            any(Pageable.class), any(String.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?title={title}&skill={skill}", "Test", "Spring Boot"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithTitleDescription() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            any(Pageable.class), any(String.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?title={title}&description={description}", "Test", "Test"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfJobsWithTitleDescriptionSkill() throws Exception {
    Job job = JobMother.complete().build();
    job.setId("2");
    job.setTitles(new HashSet<>());
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    jobDto.setId("2");
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                any(Pageable.class), any(String.class), any(String.class), any(String.class)))
        .thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(
            get(
                JOB_BASE_URL + "?title={title}&description={description}&skill={skill}",
                "Test",
                "Test",
                "Spring Boot"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfOneJob() throws Exception {
    Job job = JobMother.complete().build();
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = JobDtoMother.complete().build();
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobUseCase.findAllByPageableAndNoBadWords(any(Pageable.class))).thenReturn(jobPage);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].createdAt")
                .value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].updatedAt")
                .value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.latitude")
                .value(jobDto.getAddress().getLatitude()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.longitude")
                .value(jobDto.getAddress().getLongitude()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.street")
                .value(jobDto.getAddress().getStreet()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.houseNumber")
                .value(jobDto.getAddress().getHouseNumber()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.zip").value(jobDto.getAddress().getZip()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].address.cityTranslations").isArray())
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].address.country")
                .value(jobDto.getAddress().getCountry()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].badWordFlag").value(jobDto.getBadWordFlag()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].titles").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].descriptions").isArray())
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].jobType").value(jobDto.getJobType().toString()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].languages").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].skills").isArray());
  }

  @Test
  void readPageableListOfNoJob() throws Exception {
    Page<Job> jobPages = new PageImpl<>(Collections.emptyList());

    when(jobUseCase.findAllByPageableAndNoBadWords(any(Pageable.class))).thenReturn(jobPages);

    mock.perform(get(JOB_BASE_URL).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").doesNotExist());
  }

  @Test
  void readJob() throws Exception {
    Job job = JobMother.complete().build();
    JobDto jobDto = JobDtoMother.complete().build();
    EntityModel<JobDto> entityModel = EntityModel.of(jobDto);

    when(jobUseCase.findById("1")).thenReturn(job);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(entityModel);

    mock.perform(get(JOB_BASE_URL + "/{id}", 1).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(jobDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(jobDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(jobDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(jobDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(jobDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(jobDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.badWordFlag").value(jobDto.getBadWordFlag()))
        .andExpect(jsonPath("$.titles").isArray())
        .andExpect(jsonPath("$.descriptions").isArray())
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType().toString()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.languages").isArray())
        .andExpect(jsonPath("$.skills").isArray());
  }

  @Test
  void readJobInvalidId() throws Exception {
    when(jobUseCase.findById("1")).thenThrow(new IllegalArgumentException());

    mock.perform(get(JOB_BASE_URL + "/{id}", 1).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  void readJobNoJob() throws Exception {
    when(jobUseCase.findById("1")).thenThrow(new NoSuchElementException());

    mock.perform(get(JOB_BASE_URL + "/{id}", 1).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  void updateJobFromJson() throws Exception {
    Job job = JobMother.complete().build();
    JobDto jobDto = JobDtoMother.complete().build();
    EntityModel<JobDto> entityModel = EntityModel.of(jobDto);

    when(jobMapper.toDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.update(any(Job.class))).thenReturn(job);
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(JOB_BASE_URL + "/{id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(jobDto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(jobDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(jobDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(jobDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(jobDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(jobDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(jobDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.badWordFlag").value(jobDto.getBadWordFlag()))
        .andExpect(jsonPath("$.titles").isArray())
        .andExpect(jsonPath("$.descriptions").isArray())
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType().toString()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.languages").isArray())
        .andExpect(jsonPath("$.skills").isArray());
  }

  @Test
  void updateJobFromJsonInvalid() throws Exception {
    JobDto jobDto = JobDto.builder().build();

    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobUseCase.update(any(Job.class))).thenThrow(new IllegalArgumentException());

    mock.perform(
            patch(JOB_BASE_URL + "/{id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(jobDto)))
        .andExpect(status().isBadRequest());
  }

  @Test
  void updateJobFromJsonNotFound() throws Exception {
    Job job = JobMother.complete().build();
    JobDto jobDto = JobDtoMother.complete().build();
    EntityModel<JobDto> entityModel = EntityModel.of(jobDto);

    when(jobMapper.toDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.update(any(Job.class))).thenThrow(new NoSuchElementException());
    when(jobMapper.toDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(JOB_BASE_URL + "/{id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(jobDto)))
        .andExpect(status().isNotFound());
  }

  @Test
  void deleteJob() throws Exception {
    mock.perform(
            delete(JOB_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(JobDtoMother.complete().build())))
        .andExpect(status().is(204));

    verify(jobUseCase, times(1)).delete("1");
  }

  @Test
  void deleteJobNotFound() throws Exception {
    doThrow(new NoSuchElementException()).when(jobUseCase).delete("2");
    mock.perform(
            delete(JOB_BASE_URL + "/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson(JobDto.builder().build())))
        .andExpect(status().isNotFound());
  }

  String getJobJson(JobDto jobDto) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(jobDto);
  }
}
