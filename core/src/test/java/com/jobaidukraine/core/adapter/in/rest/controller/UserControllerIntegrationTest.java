package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.dto.*;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntityMother;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@AutoConfigureMockMvc
@Tag("integration")
@ActiveProfiles("local")
@Testcontainers
public class UserControllerIntegrationTest {
  @Autowired MockMvc mock;
  @Autowired ObjectMapper objectMapper;
  @Autowired UserRepository<UserEntity> userRepository;

  @Container
  private static final MariaDBContainer<?> database = new MariaDBContainer<>("mariadb:latest");

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", database::getJdbcUrl);
    registry.add("spring.datasource.username", database::getUsername);
    registry.add("spring.datasource.password", database::getPassword);
  }

  static final String USER_BASE_URL = "/v1.0/users/";
  private static final String USER_ID = "c4abd5a6-d228-4a5d-b460-32c074cfa17f";

  @AfterEach
  public void cleanup() {
    userRepository.deleteAll();
  }

  @Test
  void saveEmployerIntegrationTest() throws Exception {
    String json =
        objectMapper.writeValueAsString(
            UserDtoMother.completeEmployer().jobs(Collections.emptySet()).id(null).build());

    // given
    var result =
        mock.perform(
                post(USER_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse();

    // then
    mock.perform(
            get(
                USER_BASE_URL
                    + objectMapper
                        .readValue(result.getContentAsString(), EmployerDto.class)
                        .getId()))
        .andExpect(status().isOk());
  }

  @Test
  @DisplayName("input is correct, User gets saved, response 200")
  void saveUserIntegrationTest1() throws Exception {
    String json = objectMapper.writeValueAsString(UserDtoMother.completeAdmin().id(null).build());

    // given
    var result =
        mock.perform(
                post(USER_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse();

    // then
    mock.perform(
            get(
                USER_BASE_URL
                    + objectMapper.readValue(result.getContentAsString(), UserDto.class).getId()))
        .andExpect(status().isOk());
  }

  @Test
  @DisplayName("input is not correct, User does not get saved, response 400")
  void saveUserIntegrationTest2() throws Exception {
    String json =
        objectMapper.writeValueAsString(
            UserDtoMother.completeAdmin()
                .address(AddressDtoMother.germanAddress().latitude(-100F).build())
                .id(null)
                .build());

    mock.perform(post(USER_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE).content(json))
        .andExpect(status().is(400));
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("list all users if there are users, response 200")
  void listUsersIntegrationTest1() throws Exception {
    UserEntity user = UserEntityMother.completeAdmin().id(UUID.fromString(USER_ID)).build();
    userRepository.save(user);

    userRepository.save(
        UserEntityMother.completeModerator()
            .firstname("First name 1")
            .lastname("Last name 1")
            .build());
    userRepository.save(
        UserEntityMother.completeModerator()
            .firstname("First name 2")
            .lastname("Last name 2")
            .build());
    userRepository.save(
        UserEntityMother.completeModerator()
            .firstname("First name 3")
            .lastname("Last name 3")
            .build());
    userRepository.save(
        UserEntityMother.completeModerator()
            .firstname("First name 4")
            .lastname("Last name 4")
            .build());

    mock.perform(get(USER_BASE_URL).contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes", hasSize(5)))
        .andExpect(
            jsonPath("$._embedded.userDtoes[*].firstname")
                .value(
                    containsInAnyOrder(
                        "Luke", "First name 1", "First name 2", "First name 3", "First name 4")))
        .andExpect(
            jsonPath("$._embedded.userDtoes[*].lastname")
                .value(
                    containsInAnyOrder(
                        "Skywalker", "Last name 1", "Last name 2", "Last name 3", "Last name 4")));
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("show existing user with specific id, response 200")
  void showUserIntegrationTest1() throws Exception {
    UserEntity user = UserEntityMother.completeAdmin().id(UUID.fromString(USER_ID)).build();
    userRepository.save(user);

    var userId =
        userRepository
            .save(
                UserEntityMother.completeModerator()
                    .firstname("First name 1")
                    .lastname("Last name 1")
                    .build())
            .getId();

    mock.perform(get(USER_BASE_URL + userId).contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.firstname").value("First name 1"))
        .andExpect(jsonPath("$.lastname").value("Last name 1"));
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("try to find user with invalid id, response 400")
  void showUserIntegrationTest2() throws Exception {
    mock.perform(get(USER_BASE_URL + "123").contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(400));
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("try to find non existing user, response 404")
  void showUserIntegrationTest3() throws Exception {
    mock.perform(
            get(USER_BASE_URL + UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().is(404));
  }

  @Test
  @Transactional
  @WithMockUserId(userId = USER_ID)
  @DisplayName("update user with valid input, response 200")
  void updateUserIntegrationTest1() throws Exception {
    UserEntity user = UserEntityMother.completeAdmin().id(UUID.fromString(USER_ID)).build();
    userRepository.save(user);

    var userId =
        userRepository
            .save(
                UserEntityMother.completeModerator()
                    .firstname("First name 1")
                    .lastname("Last name 1")
                    .build())
            .getId();

    var toUpdate =
        UserDtoMother.completeModerator()
            .id(userId.toString())
            .firstname("First name 1")
            .lastname("Last name 1")
            .email("test@skywalker.io")
            .address(
                AddressDto.builder()
                    .latitude(51.5109688f)
                    .longitude(7.4660812f)
                    .street("Teststraße")
                    .houseNumber("1")
                    .zip("12345")
                    .cityTranslations(
                        Set.of(TranslationDtoMother.germanTranslation().value("Teststadt").build()))
                    .country("DE")
                    .build())
            .build();

    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(USER_BASE_URL + userId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.firstname").value("First name 1"))
        .andExpect(jsonPath("$.lastname").value("Last name 1"))
        .andExpect(jsonPath("$.email").value("test@skywalker.io"))
        .andExpect(jsonPath("$.address.street").value("Teststraße"))
        .andExpect(jsonPath("$.address.houseNumber").value("1"))
        .andExpect(jsonPath("$.address.zip").value("12345"))
        .andExpect(jsonPath("$.address.cityTranslations[0].value").value("Teststadt"))
        .andExpect(jsonPath("$.address.country").value("DE"));

    var updatedUser = userRepository.findById(userId).get();

    assertThat(updatedUser.getFirstname()).isEqualTo("First name 1");
    assertThat(updatedUser.getLastname()).isEqualTo("Last name 1");
    assertThat(updatedUser.getEmail()).isEqualTo("test@skywalker.io");
    assertThat(updatedUser.getAddress().getStreet()).isEqualTo("Teststraße");
    assertThat(updatedUser.getAddress().getHouseNumber()).isEqualTo("1");
    assertThat(updatedUser.getAddress().getZip()).isEqualTo("12345");
    assertThat(new ArrayList<>(updatedUser.getAddress().getCityTranslations()).get(0).getValue())
        .isEqualTo("Teststadt");
    assertThat(updatedUser.getAddress().getCountry()).isEqualTo("DE");
  }

  @Test
  @Transactional
  @WithMockUserId(userId = USER_ID)
  @DisplayName("update user with invalid input, response 400")
  void updateUserIntegrationTest2() throws Exception {
    UserEntity user = UserEntityMother.completeAdmin().id(UUID.fromString(USER_ID)).build();
    userRepository.save(user);

    UserDto toUpdate =
        UserDtoMother.completeModerator()
            .id(user.getId().toString())
            .address(AddressDto.builder().latitude(-100f).build())
            .build();

    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(USER_BASE_URL + user.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().is(400));
  }

  @Test
  @Transactional
  @WithMockUserId(userId = USER_ID)
  @DisplayName("update user with invalid id, response 404")
  void updateUserIntegrationTest3() throws Exception {
    UserDto toUpdate =
        UserDtoMother.completeModerator()
            .id(UUID.randomUUID().toString())
            .address(AddressDto.builder().latitude(-100f).build())
            .build();

    String json = objectMapper.writeValueAsString(toUpdate);

    mock.perform(
            patch(USER_BASE_URL + toUpdate.getId().toString())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(status().is(400));
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("delete existing job, response 204")
  void deleteUserIntegrationTest1() throws Exception {
    UserEntity user1 = UserEntityMother.completeAdmin().build();
    UserEntity user2 = userRepository.save(user1);

    mock.perform(delete(USER_BASE_URL + user2.getId().toString())).andExpect(status().is(204));
    ;
  }

  @Test
  @WithMockUserId(userId = USER_ID)
  @DisplayName("try to delete non existing job, response 404")
  void deleteUserIntegrationTest2() throws Exception {
    mock.perform(delete(USER_BASE_URL + UUID.randomUUID().toString())).andExpect(status().is(404));
  }
}
