package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.UserResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import com.jobaidukraine.core.adapter.in.rest.dto.UserDtoMother;
import com.jobaidukraine.core.adapter.in.rest.mapper.UserMapper;
import com.jobaidukraine.core.domain.UserMother;
import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.services.ports.in.UserUseCase;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(UserController.class)
@WithMockUser(
    username = "Picard",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class UserControllerTest {

  @Autowired MockMvc mock;
  @MockBean UserUseCase userUseCase;
  @MockBean PagedResourcesAssembler<UserDto> pagedResourcesAssembler;
  @MockBean UserMapper userMapper;
  @MockBean UserResourceAssembler userResourceAssembler;

  static final String USER_BASE_URL = "/v1.0/users";
  static final DateTimeFormatter TIMESTAMP_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
  static final DateTimeFormatter TIMESTAMP_FORMATTER_DATE =
      DateTimeFormatter.ofPattern("yyyy-MM-dd");

  @Test
  void createUserFromJson() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();

    when(userMapper.toDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.save(any(User.class))).thenReturn(user);
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);

    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(EntityModel.of(userDto));

    mock.perform(
            post(USER_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(userDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(userDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(userDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(userDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(userDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(userDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(
            jsonPath("$.dateOfBirth")
                .value(TIMESTAMP_FORMATTER_DATE.format(userDto.getDateOfBirth())))
        .andExpect(jsonPath("$.originCountry").value(userDto.getOriginCountry()));
  }

  @Test
  void createUserFromJsonInvalid() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();

    when(userMapper.toDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.save(any(User.class))).thenThrow(new IllegalArgumentException());
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);

    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(EntityModel.of(userDto));

    mock.perform(
            post(USER_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isBadRequest());
  }

  @Test
  void readPagableListOfUsers() throws Exception {
    User user = getMockUser();
    User user2 = getMockUser();
    user2.setId("333z3237-e89b-12d3-a456-426614174000");
    Page<User> userPage = new PageImpl<>(List.of(user, user2));
    UserDto userDto = getMockUserDto();
    UserDto userDto2 = getMockUserDto();
    userDto2.setId("333z3237-e89b-12d3-a456-426614174000");

    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto, userDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userUseCase.findAllByPageable(any(Pageable.class))).thenReturn(userPage);
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes").isArray())
        .andExpect(jsonPath("$._embedded.userDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.userDtoes[0].id").value(userDto.getId()))
        .andExpect(jsonPath("$._embedded.userDtoes[1].id").value(userDto2.getId()));
  }

  @Test
  void readPagableListOfOneUser() throws Exception {
    User user = getMockUser();
    Page<User> userPage = new PageImpl<>(List.of(user));
    UserDto userDto = getMockUserDto();
    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userUseCase.findAllByPageable(any(Pageable.class))).thenReturn(userPage);
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes[0].id").value(userDto.getId()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].role").value(userDto.getRole().name()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].createdAt")
                .value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].updatedAt")
                .value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.latitude")
                .value(userDto.getAddress().getLatitude()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.longitude")
                .value(userDto.getAddress().getLongitude()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.street")
                .value(userDto.getAddress().getStreet()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.houseNumber")
                .value(userDto.getAddress().getHouseNumber()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.zip").value(userDto.getAddress().getZip()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].address.cityTranslations").isArray())
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].address.country")
                .value(userDto.getAddress().getCountry()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].email").value(userDto.getEmail()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].lastname").value(userDto.getLastname()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].dateOfBirth")
                .value(TIMESTAMP_FORMATTER_DATE.format(userDto.getDateOfBirth())))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].originCountry").value(userDto.getOriginCountry()));
  }

  @Test
  void readPagableListOfOneUserInvalid() throws Exception {
    UserDto userDto = getMockUserDto();
    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userUseCase.findAllByPageable(any(Pageable.class)))
        .thenThrow(new IllegalArgumentException());
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL)).andExpect(status().isBadRequest());
  }

  @Test
  void readPagableListOfOneUserNotFound() throws Exception {
    UserDto userDto = getMockUserDto();
    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userUseCase.findAllByPageable(any(Pageable.class)))
        .thenThrow(new NoSuchElementException());
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL)).andExpect(status().isNotFound());
  }

  @Test
  void readPageableListOfNoUser() throws Exception {
    Page<User> userPages = new PageImpl<>(Collections.emptyList());

    when(userUseCase.findAllByPageable(any(Pageable.class))).thenReturn(userPages);

    mock.perform(get(USER_BASE_URL).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes").doesNotExist());
  }

  @Test
  void readUser() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userUseCase.findById("112z3237-e89b-12d3-a456-426614174000")).thenReturn(user);
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(
            get(USER_BASE_URL + "/{id}", "112z3237-e89b-12d3-a456-426614174000")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(userDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(userDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(userDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(userDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(userDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(userDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(
            jsonPath("$.dateOfBirth")
                .value(TIMESTAMP_FORMATTER_DATE.format(userDto.getDateOfBirth())))
        .andExpect(jsonPath("$.originCountry").value(userDto.getOriginCountry()));
  }

  @Test
  void updateUserFromJson() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userMapper.toDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.update(any(User.class))).thenReturn(user);
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(USER_BASE_URL + "/{id}", "112z3237-e89b-12d3-a456-426614174000")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.address.latitude").value(userDto.getAddress().getLatitude()))
        .andExpect(jsonPath("$.address.longitude").value(userDto.getAddress().getLongitude()))
        .andExpect(jsonPath("$.address.street").value(userDto.getAddress().getStreet()))
        .andExpect(jsonPath("$.address.houseNumber").value(userDto.getAddress().getHouseNumber()))
        .andExpect(jsonPath("$.address.zip").value(userDto.getAddress().getZip()))
        .andExpect(jsonPath("$.address.cityTranslations").isArray())
        .andExpect(jsonPath("$.address.country").value(userDto.getAddress().getCountry()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(
            jsonPath("$.dateOfBirth")
                .value(TIMESTAMP_FORMATTER_DATE.format(userDto.getDateOfBirth())))
        .andExpect(jsonPath("$.originCountry").value(userDto.getOriginCountry()));
  }

  @Test
  void updateUserFromJsonInvalid() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userMapper.toDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.update(any(User.class))).thenThrow(new IllegalArgumentException());
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(USER_BASE_URL + "/{id}", "112z3237-e89b-12d3-a456-426614174000")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isBadRequest());
  }

  @Test
  void updateUserFromJsonNotFound() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userMapper.toDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.update(any(User.class))).thenThrow(new NoSuchElementException());
    when(userMapper.toDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(USER_BASE_URL + "/{id}", "112z3237-e89b-12d3-a456-426614174000")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isNotFound());
  }

  @Test
  void deleteUser() throws Exception {
    mock.perform(
            delete(USER_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().is(204));

    verify(userUseCase, times(1)).delete("1");
  }

  @Test
  void deleteUserNotFound() throws Exception {
    doThrow(new NoSuchElementException()).when(userUseCase).delete("1");
    mock.perform(
            delete(USER_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isNotFound());
  }

  User getMockUser() {
    return UserMother.completeAdmin().build();
  }

  UserDto getMockUserDto() {
    return UserDtoMother.completeAdmin().build();
  }

  String getUserJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(getMockUserDto());
  }
}
