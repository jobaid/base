package com.jobaidukraine.core.adapter.in.rest.controller;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockCustomUserSecurityContextFactory
    implements WithSecurityContextFactory<WithMockUserId> {
  @Override
  public SecurityContext createSecurityContext(WithMockUserId mockUserId) {
    SecurityContext context = SecurityContextHolder.createEmptyContext();

    String token =
        Jwts.builder()
            .setHeaderParam("alg", "HS256")
            .setHeaderParam("typ", "JWT")
            .claim("userId", mockUserId.userId())
            .compact();

    Jwt principal =
        Jwt.withTokenValue(token)
            .header("alg", "HS256")
            .header("typ", "JWT")
            .claim("userId", mockUserId.userId())
            .build();

    Authentication auth = new TestingAuthenticationToken(principal, "password");

    context.setAuthentication(auth);
    return context;
  }
}
