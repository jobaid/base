package com.jobaidukraine.core.adapter.in.rest.controller;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.springframework.security.test.context.support.WithSecurityContext;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithMockUserId {

  String userId() default "c4abd5a6-d228-4a5d-b460-32c074cfa17f";
}
