package com.jobaidukraine.core.adapter.in.rest.dto;

import java.util.Set;

public class AddressDtoMother {
  public static AddressDto.AddressDtoBuilder germanAddress() {
    return AddressDto.builder()
        .latitude(51.5109688f)
        .longitude(7.4660812f)
        .street("Friedensplatz")
        .houseNumber("1337")
        .zip("44315")
        .cityTranslations(
            Set.of(TranslationDtoMother.germanTranslation().value("Dortmund").build()))
        .country("DE");
  }
}
