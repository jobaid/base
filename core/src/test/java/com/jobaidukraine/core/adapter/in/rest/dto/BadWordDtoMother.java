package com.jobaidukraine.core.adapter.in.rest.dto;

import java.util.Set;
import java.util.UUID;

public class BadWordDtoMother {
  public static BadWordDto.BadWordDtoBuilder germanBadWord() {
    return BadWordDto.builder()
        .id(UUID.randomUUID().toString())
        .wordTranslations(
            Set.of(TranslationDtoMother.germanTranslation().value("Schimpfwort").build()));
  }

  public static BadWordDto.BadWordDtoBuilder englishBadWord() {
    return BadWordDto.builder()
        .id(UUID.randomUUID().toString())
        .wordTranslations(
            Set.of(TranslationDtoMother.englishTranslation().value("Swear word").build()));
  }
}
