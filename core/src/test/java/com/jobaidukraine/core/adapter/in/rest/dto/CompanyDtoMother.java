package com.jobaidukraine.core.adapter.in.rest.dto;

public class CompanyDtoMother {
  public static CompanyDto.CompanyDtoBuilder germanCompany() {
    return CompanyDto.builder()
        .name("Adesso")
        .url("www.adesso.de")
        .email("info@adesso.de")
        .logo("test-logo");
  }
}
