package com.jobaidukraine.core.adapter.in.rest.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class EmployerDtoMother {
  public static EmployerDto.EmployerDtoBuilder complete() {
    return EmployerDto.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleDto.EMPLOYER)
        .address(AddressDtoMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .company(CompanyDtoMother.germanCompany().build())
        .jobs(Set.of(JobDtoMother.complete().build()));
  }
}
