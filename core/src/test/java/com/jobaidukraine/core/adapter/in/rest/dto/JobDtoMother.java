package com.jobaidukraine.core.adapter.in.rest.dto;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class JobDtoMother {
  public static JobDto.JobDtoBuilder complete() {
    return JobDto.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .address(AddressDtoMother.germanAddress().build())
        .badWordFlag(false)
        .titles(Set.of(TranslationDtoMother.germanTranslation().value("Test").build()))
        .descriptions(Set.of(TranslationDtoMother.germanTranslation().value("Test").build()))
        .jobType(JobTypeDto.FREELANCE)
        .applicationLink("https://jobaidukraine.com/apply")
        .applicationMail("apply@jobaidukraine.com")
        .videoUrl("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
        .languages(Set.of(LanguageSkillDtoMother.germanFluent().build()))
        .skills(Set.of(SkillDtoMother.professionalSkill().name("Spring Boot").build()));
  }
}
