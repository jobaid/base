package com.jobaidukraine.core.adapter.in.rest.dto;

import java.util.Locale;
import org.testcontainers.shaded.org.apache.commons.lang3.LocaleUtils;

public class LanguageSkillDtoMother {
  public static LanguageSkillDto.LanguageSkillDtoBuilder germanFluent() {
    return LanguageSkillDto.builder().locale(Locale.GERMAN).level(LanguageLevelDto.FLUENT);
  }

  public static LanguageSkillDto.LanguageSkillDtoBuilder ukraineNative() {
    return LanguageSkillDto.builder()
        .locale(LocaleUtils.toLocale("uk_UA"))
        .level(LanguageLevelDto.NATIVE);
  }
}
