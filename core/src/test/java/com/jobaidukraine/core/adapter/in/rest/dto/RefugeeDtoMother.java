package com.jobaidukraine.core.adapter.in.rest.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class RefugeeDtoMother {
  public static RefugeeDto.RefugeeDtoBuilder complete() {
    return RefugeeDto.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleDto.REFUGEE)
        .address(AddressDtoMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .languages(Set.of(LanguageSkillDtoMother.germanFluent().build()))
        .skills(Set.of(SkillDtoMother.professionalSkill().name("Spring Boot").build()));
  }
}
