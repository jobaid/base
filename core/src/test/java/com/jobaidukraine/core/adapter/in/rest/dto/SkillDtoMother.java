package com.jobaidukraine.core.adapter.in.rest.dto;

public class SkillDtoMother {
  static SkillDto.SkillDtoBuilder professionalSkill() {
    return SkillDto.builder().name("Professional").level(SkillLevelDto.PROFESSIONAL);
  }
}
