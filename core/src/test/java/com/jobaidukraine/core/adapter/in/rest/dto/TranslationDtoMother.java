package com.jobaidukraine.core.adapter.in.rest.dto;

import java.time.LocalDateTime;
import java.util.Locale;

public class TranslationDtoMother {
  public static TranslationDto.TranslationDtoBuilder germanTranslation() {
    return TranslationDto.builder()
        .locale(Locale.GERMAN)
        .type(TranslationTypeDto.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }

  public static TranslationDto.TranslationDtoBuilder englishTranslation() {
    return TranslationDto.builder()
        .locale(Locale.ENGLISH)
        .type(TranslationTypeDto.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }
}
