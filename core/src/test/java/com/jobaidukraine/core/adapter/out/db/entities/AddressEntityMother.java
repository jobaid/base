package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;

public class AddressEntityMother {
  static AddressEntity.AddressEntityBuilder germanAddress() {
    return AddressEntity.builder()
        .latitude(51.5109688f)
        .longitude(7.4660812f)
        .street("Friedensplatz")
        .houseNumber("1337")
        .zip("44315")
        .cityTranslations(
            Set.of(TranslationEntityMother.germanTranslation().value("Dortmund").build()))
        .country("DE");
  }
}
