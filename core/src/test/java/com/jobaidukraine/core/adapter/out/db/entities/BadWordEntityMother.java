package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Set;
import java.util.UUID;

public class BadWordEntityMother {
  public static BadWordEntity.BadWordEntityBuilder germanBadWord() {
    return BadWordEntity.builder()
        .id(UUID.randomUUID())
        .wordTranslations(
            Set.of(TranslationEntityMother.germanTranslation().value("Schimpfwort").build()));
  }

  public static BadWordEntity.BadWordEntityBuilder englishBadWord() {
    return BadWordEntity.builder()
        .id(UUID.randomUUID())
        .wordTranslations(
            Set.of(TranslationEntityMother.englishTranslation().value("Swear word").build()));
  }
}
