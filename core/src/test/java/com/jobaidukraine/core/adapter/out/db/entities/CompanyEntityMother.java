package com.jobaidukraine.core.adapter.out.db.entities;

public class CompanyEntityMother {
  public static CompanyEntity.CompanyEntityBuilder germanCompany() {
    return CompanyEntity.builder()
        .name("Adesso")
        .url("www.adesso.de")
        .email("info@adesso.de")
        .logo("test-logo");
  }
}
