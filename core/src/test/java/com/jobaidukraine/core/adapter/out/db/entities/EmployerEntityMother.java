package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class EmployerEntityMother {
  public static EmployerEntity.EmployerEntityBuilder complete() {
    return EmployerEntity.builder()
        .id(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleEntity.EMPLOYER)
        .address(AddressEntityMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .company(CompanyEntityMother.germanCompany().build())
        .jobs(Set.of(JobEntityMother.complete().build()));
  }
}
