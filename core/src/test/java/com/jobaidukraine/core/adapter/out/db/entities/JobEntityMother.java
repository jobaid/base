package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class JobEntityMother {
  public static JobEntity.JobEntityBuilder complete() {
    return JobEntity.builder()
        .id(UUID.randomUUID())
        .employerId(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .address(AddressEntityMother.germanAddress().build())
        .badWordFlag(false)
        .titles(Set.of(TranslationEntityMother.germanTranslation().value("Test").build()))
        .descriptions(Set.of(TranslationEntityMother.germanTranslation().value("Test").build()))
        .jobType(JobTypeEntity.FREELANCE)
        .applicationLink("https://jobaidukraine.com/apply")
        .applicationMail("apply@jobaidukraine.com")
        .videoUrl("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
        .languages(Set.of(LanguageSkillEntityMother.germanFluent().build()))
        .skills(Set.of(SkillEntityMother.professionalSkill().name("Spring Boot").build()));
  }
}
