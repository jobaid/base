package com.jobaidukraine.core.adapter.out.db.entities;

import java.util.Locale;

public class LanguageSkillEntityMother {
  static LanguageSkillEntity.LanguageSkillEntityBuilder germanFluent() {
    return LanguageSkillEntity.builder().locale(Locale.GERMAN).level(LanguageLevelEntity.FLUENT);
  }
}
