package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class RefugeeEntityMother {
  public static RefugeeEntity.RefugeeEntityBuilder complete() {
    return RefugeeEntity.builder()
        .id(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleEntity.REFUGEE)
        .address(AddressEntityMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .languages(Set.of(LanguageSkillEntityMother.germanFluent().build()))
        .skills(Set.of(SkillEntityMother.professionalSkill().name("Spring Boot").build()));
  }
}
