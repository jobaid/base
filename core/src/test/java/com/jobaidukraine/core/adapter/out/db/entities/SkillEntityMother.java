package com.jobaidukraine.core.adapter.out.db.entities;

public class SkillEntityMother {
  static SkillEntity.SkillEntityBuilder professionalSkill() {
    return SkillEntity.builder().name("Professional").level(SkillLevelEntity.PROFESSIONAL);
  }
}
