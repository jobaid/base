package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDateTime;
import java.util.Locale;

public class TranslationEntityMother {
  static TranslationEntity.TranslationEntityBuilder germanTranslation() {
    return TranslationEntity.builder()
        .locale(Locale.GERMAN)
        .type(TranslationTypeEntity.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }

  static TranslationEntity.TranslationEntityBuilder englishTranslation() {
    return TranslationEntity.builder()
        .locale(Locale.ENGLISH)
        .type(TranslationTypeEntity.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }
}
