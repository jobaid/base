package com.jobaidukraine.core.adapter.out.db.entities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class UserEntityMother {

  public static UserEntity.UserEntityBuilder completeAdmin() {
    return UserEntity.builder()
        .id(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleEntity.ADMIN)
        .address(AddressEntityMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine");
  }

  public static UserEntity.UserEntityBuilder completeModerator() {
    return UserEntity.builder()
        .id(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleEntity.MODERATOR)
        .address(AddressEntityMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine");
  }

  public static EmployerEntity.EmployerEntityBuilder completeEmployer() {
    return EmployerEntity.builder()
        .id(UUID.randomUUID())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(RoleEntity.EMPLOYER)
        .address(AddressEntityMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .company(CompanyEntityMother.germanCompany().build())
        .jobs(Set.of(JobEntityMother.complete().build()));
  }
}
