package com.jobaidukraine.core.adapter.out.db.implementation.badword;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;

import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntity;
import com.jobaidukraine.core.adapter.out.db.entities.BadWordEntityMother;
import com.jobaidukraine.core.adapter.out.db.mapper.BadWordMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.TranslationMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.BadWordRepository;
import com.jobaidukraine.core.domain.BadWordMother;
import com.jobaidukraine.core.domain.values.BadWord;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class BadWordAdapterTest {

  @Mock private BadWordRepository badWordRepository;
  @Mock private BadWordMapper badWordMapper;
  @Mock private TranslationMapper translationMapper;
  @InjectMocks private BadWordAdapter badWordAdapter;

  @Test
  void findById() {
    BadWordEntity badWordEntity =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();

    Mockito.when(
            badWordRepository.findById(UUID.fromString("00000000-0000-0000-0000-000000000000")))
        .thenReturn(Optional.ofNullable(badWordEntity));
    Mockito.when(badWordMapper.toDomain(argThat(new BadWordEntityMatcher(badWordEntity))))
        .thenReturn(badWord);

    Optional<BadWord> result = badWordAdapter.findById("00000000-0000-0000-0000-000000000000");

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(badWord);
  }

  @Test
  void findAll() {
    List<BadWordEntity> badWordEntities =
        List.of(
            BadWordEntityMother.germanBadWord()
                .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
                .build(),
            BadWordEntityMother.englishBadWord()
                .id(UUID.fromString("00000000-0000-0000-0000-000000000001"))
                .build());

    List<BadWord> badWords =
        List.of(
            BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build(),
            BadWordMother.englishBadWord().id("00000000-0000-0000-0000-000000000001").build());

    Mockito.when(badWordRepository.findAll()).thenReturn(badWordEntities);

    Mockito.when(badWordMapper.toDomainList(any())).thenReturn(badWords);

    List<BadWord> result = badWordAdapter.findAll();

    Assertions.assertThat(result)
        .isNotEmpty()
        .containsExactlyInAnyOrder(badWords.get(0), badWords.get(1));
  }

  @Test
  void findAllWithPageable() {
    BadWordEntity badWordEntity1 =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWordEntity badWordEntity2 =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000001"))
            .build();
    BadWord badWord1 =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();
    BadWord badWord2 =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000001").build();

    Mockito.when(badWordRepository.findAll(any(Pageable.class)))
        .thenReturn(new PageImpl<>(List.of(badWordEntity1, badWordEntity2)));

    Mockito.when(badWordMapper.toDomain(argThat(new BadWordEntityMatcher(badWordEntity1))))
        .thenReturn(badWord1);
    Mockito.when(badWordMapper.toDomain(argThat(new BadWordEntityMatcher(badWordEntity2))))
        .thenReturn(badWord2);

    Page<BadWord> result = badWordAdapter.findAll(Pageable.ofSize(2));

    Assertions.assertThat(result).isNotEmpty().containsExactlyInAnyOrder(badWord1, badWord2);
  }

  @Test
  void saveBadWord() {
    BadWordEntity badWordEntity =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();

    Mockito.when(badWordMapper.toEntity(badWord)).thenReturn(badWordEntity);
    Mockito.when(badWordRepository.save(argThat(new BadWordEntityMatcher(badWordEntity))))
        .thenReturn(badWordEntity);
    Mockito.when(badWordMapper.toDomain(argThat(new BadWordEntityMatcher(badWordEntity))))
        .thenReturn(badWord);

    BadWord result = badWordAdapter.saveBadWord(badWord);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(badWord);
  }

  @Test
  void updateBadWord() {
    BadWordEntity badWordEntity =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();

    Mockito.when(
            badWordRepository.findById(UUID.fromString("00000000-0000-0000-0000-000000000000")))
        .thenReturn(Optional.of(badWordEntity));

    Mockito.when(translationMapper.toEntitySet(badWord.getWordTranslations()))
        .thenReturn(badWordEntity.getWordTranslations());

    Mockito.when(badWordRepository.save(badWordEntity)).thenReturn(badWordEntity);
    Mockito.when(badWordMapper.toDomain(badWordEntity)).thenReturn(badWord);

    BadWord result = badWordAdapter.updateBadWord(badWord);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(badWord);
  }

  @Test
  void updateBadWordWithoutTranslations() {
    BadWordEntity badWordEntity =
        BadWordEntityMother.germanBadWord()
            .wordTranslations(Collections.emptySet())
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWordEntity withTranslation =
        BadWordEntityMother.germanBadWord()
            .id(UUID.fromString("00000000-0000-0000-0000-000000000000"))
            .build();
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();

    Mockito.when(
            badWordRepository.findById(UUID.fromString("00000000-0000-0000-0000-000000000000")))
        .thenReturn(Optional.of(badWordEntity));

    Mockito.when(translationMapper.toEntitySet(badWord.getWordTranslations()))
        .thenReturn(withTranslation.getWordTranslations());

    Mockito.when(badWordRepository.save(argThat(new BadWordEntityMatcher(withTranslation))))
        .thenReturn(withTranslation);
    Mockito.when(badWordMapper.toDomain(argThat(new BadWordEntityMatcher(withTranslation))))
        .thenReturn(badWord);

    BadWord result = badWordAdapter.updateBadWord(badWord);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(badWord);
  }

  @Test
  void deleteBadWord() {
    badWordAdapter.deleteBadWord("00000000-0000-0000-0000-000000000000");
    Mockito.verify(badWordRepository)
        .deleteById(UUID.fromString("00000000-0000-0000-0000-000000000000"));
  }

  private static class BadWordEntityMatcher implements ArgumentMatcher<BadWordEntity> {
    private final BadWordEntity expected;

    public BadWordEntityMatcher(BadWordEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(BadWordEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getId(), expected.getId())
          && Objects.equals(actual.getWordTranslations(), expected.getWordTranslations());
    }
  }

  private static class BadWordListEntityMatcher implements ArgumentMatcher<List<BadWordEntity>> {
    private final List<BadWordEntity> expected;

    public BadWordListEntityMatcher(List<BadWordEntity> expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(List<BadWordEntity> actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      if (expected.size() != actual.size()) {
        return false;
      }

      for (int i = 0; i < expected.size(); i++) {
        if (!Objects.equals(actual.get(i).getId(), expected.get(i).getId())
            || Objects.equals(
                actual.get(i).getWordTranslations(), expected.get(i).getWordTranslations())) {
          return false;
        }
      }

      return true;
    }
  }
}
