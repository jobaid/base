package com.jobaidukraine.core.adapter.out.db.implementation.job;

import static org.assertj.core.api.Assertions.assertThat;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntityMother;
import com.jobaidukraine.core.adapter.out.db.mapper.JobMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.SkillMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.TranslationMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.JobMother;
import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.util.Optional;
import java.util.UUID;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Tag("integration")
@ActiveProfiles("local")
@Transactional
@Testcontainers
public class JobAdapterIntegrationTest {

  @Autowired JobPort jobPort;
  @Autowired JobRepository jobRepository;
  @Autowired JobMapper jobMapper;
  @Autowired TranslationMapper translationMapper;
  @Autowired SkillMapper skillMapper;

  String FIX_UUID = "00000000-0000-0000-0000-000000000000";

  @Container
  private static final MariaDBContainer<?> database = new MariaDBContainer<>("mariadb:latest");

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", database::getJdbcUrl);
    registry.add("spring.datasource.username", database::getUsername);
    registry.add("spring.datasource.password", database::getPassword);
  }

  @Test
  @DisplayName("try to save Job, with valid input")
  void saveTest1() {
    // arrange
    Job job = JobMother.complete().employerId(FIX_UUID).build();

    // act
    Job result = jobPort.save(job);

    // assert
    assertThat(result).isNotNull();
    assertThat(result.getVideoUrl()).isEqualTo(job.getVideoUrl());
  }

  @Test
  @DisplayName("try to save Job without assigned employer")
  void saveTest2() {
    // arrange
    Job job = JobMother.complete().employerId(null).build();

    // act & assert
    Assert.assertThrows(DataIntegrityViolationException.class, () -> jobPort.save(job));
  }

  @Test
  @DisplayName("try to find existing job by id")
  void findByIdTest1() {
    // arrange
    JobEntity saved =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Job result = jobPort.findById(saved.getId().toString()).orElseThrow();

    // assert
    assertThat(result).isEqualTo(jobMapper.toDomain(saved));
  }

  @Test
  @DisplayName("try to find non existing job by id")
  void findByIdTest2() {
    // arrange & act
    Optional<Job> result = jobPort.findById(FIX_UUID);

    // assert
    assertThat(result).isNotPresent();
  }

  @Test
  @DisplayName("find all jobs by pageable when jobs exist")
  void findAllByPageableTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result = jobPort.findAllByPageable(PageRequest.ofSize(1));

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
  }

  @Test
  @DisplayName("find all jobs by pageable when no jobs exist")
  void findAllByPageableTest2() {
    // arrange & act
    Page<Job> result = jobPort.findAllByPageable(PageRequest.ofSize(1));

    // assert
    assertThat(result).isEmpty();
    ;
  }

  @Test
  @DisplayName("find all jobs by pageable, titles, description or skill when jobs exist")
  void findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                PageRequest.ofSize(1), "", "", "Spring Boot");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getTitles())
        .isEqualTo(translationMapper.toDomainSet(job.getTitles()));
    assertThat(result.getContent().get(0).getDescriptions())
        .isEqualTo(translationMapper.toDomainSet(job.getDescriptions()));
    assertThat(result.getContent().get(0).getSkills())
        .isEqualTo(skillMapper.toDomainSet(job.getSkills()));
  }

  @Test
  @DisplayName("find all jobs by pageable, title, description or skill when no jobs exist")
  void findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                PageRequest.ofSize(1), "", "", "Spring Boot");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable, title or description when jobs exist")
  void findAllByTitlesValueContainingOrDescriptionsValueContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "test");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getTitles())
        .isEqualTo(translationMapper.toDomainSet(job.getTitles()));
    assertThat(result.getContent().get(0).getDescriptions())
        .isEqualTo(translationMapper.toDomainSet(job.getDescriptions()));
  }

  @Test
  @DisplayName("find all jobs by pageable, title or description when no jobs exist")
  void findAllByTitlesValueContainingOrDescriptionsValueContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "test");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable, title or skill when jobs exist")
  void findAllByTitlesValueContainingOrSkillsNameContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "Spring Boot");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getTitles())
        .isEqualTo(translationMapper.toDomainSet(job.getTitles()));
    assertThat(result.getContent().get(0).getSkills())
        .isEqualTo(skillMapper.toDomainSet(job.getSkills()));
  }

  @Test
  @DisplayName("find all jobs by pageable, title or skill when no jobs exist")
  void findAllByTitlesValueContainingOrSkillsNameContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "Spring Boot");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable, description or skill when jobs exist")
  void findAllByDescriptionsValueContainingOrSkillsNameContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "Spring Boot");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getDescriptions())
        .isEqualTo(translationMapper.toDomainSet(job.getDescriptions()));
    assertThat(result.getContent().get(0).getSkills())
        .isEqualTo(skillMapper.toDomainSet(job.getSkills()));
  }

  @Test
  @DisplayName("find all jobs by pageable, description or skill when no jobs exist")
  void findAllByDescriptionsValueContainingOrSkillsNameContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.ofSize(1), "", "Spring Boot");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable and title when jobs exist")
  void findAllByTitlesValueContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingAndNoBadWords(PageRequest.ofSize(1), "test");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getTitles())
        .isEqualTo(translationMapper.toDomainSet(job.getTitles()));
  }

  @Test
  @DisplayName("find all jobs by pageable and title when no jobs exist")
  void findAllByTitlesValueContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllByTitlesValueContainingAndNoBadWords(PageRequest.ofSize(1), "test");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable and description when jobs exist")
  void findAllByDescriptionsValueContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllByDescriptionsValueContainingAndNoBadWords(PageRequest.ofSize(1), "test");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getDescriptions())
        .isEqualTo(translationMapper.toDomainSet(job.getDescriptions()));
  }

  @Test
  @DisplayName("find all jobs by pageable and description when no jobs exist")
  void findAllByDescriptionsValueContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllByDescriptionsValueContainingAndNoBadWords(PageRequest.ofSize(1), "test");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("find all jobs by pageable and skill when jobs exist")
  void findAllBySkillsNameContainingTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    Page<Job> result =
        jobPort.findAllBySkillsNameContainingAndNoBadWords(PageRequest.ofSize(1), "Spring Boot");

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(job.getId().toString());
    assertThat(result.getContent().get(0).getSkills())
        .isEqualTo(skillMapper.toDomainSet(job.getSkills()));
  }

  @Test
  @DisplayName("find all jobs by pageable and skill when no jobs exist")
  void findAllBySkillsNameContainingTest2() {
    // arrange & act
    Page<Job> result =
        jobPort.findAllBySkillsNameContainingAndNoBadWords(PageRequest.ofSize(1), "Spring Boot");

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("update job with valid input")
  void updateTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());
    Job toUpdate =
        JobMother.complete()
            .id(job.getId().toString())
            .videoUrl("www.google.com")
            .applicationLink("www.adesso.de")
            .build();

    // act
    Job result = jobPort.update(toUpdate);

    // assert
    assertThat(result.getId()).isEqualTo(toUpdate.getId());
    assertThat(result.getVideoUrl()).isEqualTo(toUpdate.getVideoUrl());
    assertThat(result.getApplicationLink()).isEqualTo(toUpdate.getApplicationLink());
  }

  @Test
  @DisplayName("update job with invalid input")
  void updateTest2() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());
    Job toUpdate = JobMother.complete().id(job.getId().toString()).videoUrl(null).build();

    // act
    Job result = jobPort.update(toUpdate);

    // assert
    assertThat(result.getId()).isEqualTo(toUpdate.getId());
    assertThat(result.getVideoUrl()).isNotEqualTo(toUpdate.getVideoUrl());
  }

  @Test
  @DisplayName("delete existing job")
  void deleteTest1() {
    // arrange
    JobEntity job =
        jobRepository.save(
            JobEntityMother.complete().employerId(UUID.fromString(FIX_UUID)).build());

    // act
    jobPort.delete(job.getId().toString());

    // assert
    assertThat(jobRepository.findById(job.getId())).isNotPresent();
  }

  @Test
  @DisplayName("try to delete non existing job")
  void deleteTest2() {
    // arrange & act & assert
    Assert.assertThrows(EmptyResultDataAccessException.class, () -> jobPort.delete(FIX_UUID));
  }
}
