package com.jobaidukraine.core.adapter.out.db.implementation.job;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntityMother;
import com.jobaidukraine.core.adapter.out.db.mapper.*;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.JobMother;
import com.jobaidukraine.core.domain.entities.Job;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class JobAdapterTest {

  @Mock JobRepository jobRepository;
  @Mock JobMapper jobMapper;
  @Mock TranslationMapper translationMapper;
  @Mock JobTypeMapper jobTypeMapper;
  @Mock LanguageSkillMapper languageSkillMapper;
  @Mock SkillMapper skillMapper;
  @Mock AddressMapper addressMapper;

  @InjectMocks JobAdapter jobAdapter;

  @Test
  void findExistingJobById() {
    JobEntity jobEntity = JobEntityMother.complete().build();
    Job job = JobMother.complete().id(jobEntity.getId().toString()).build();

    when(jobRepository.findById(jobEntity.getId())).thenReturn(Optional.of(jobEntity));
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(job);

    Optional<Job> result = jobAdapter.findById(jobEntity.getId().toString());

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(job);
  }

  @Test
  void findAllExistingJobsByPageableWithBadWords() {
    JobEntity jobEntity1 = JobEntityMother.complete().badWordFlag(true).build();
    JobEntity jobEntity2 = JobEntityMother.complete().badWordFlag(true).build();
    Job job1 = JobMother.complete().badWordFlag(true).id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().badWordFlag(true).id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAll(PageRequest.of(0, 10))).thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result = jobAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAllExistingJobsByPageable() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAll(PageRequest.of(0, 10))).thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result = jobAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContaining() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
                PageRequest.of(0, 10), "Test", "Test", "Spring Boot", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                PageRequest.of(0, 10), "Test", "Test", "Spring Boot");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAllByTitlesValueContainingOrDescriptionsValueContaining() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllByTitlesValueContainingOrDescriptionsValueContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", "Test", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            PageRequest.of(0, 10), "Test", "Test");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAllByTitlesValueContainingOrSkillsNameContaining() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllByTitlesValueContainingOrSkillsNameContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", "Spring Boot", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.of(0, 10), "Test", "Spring Boot");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAllByDescriptionsValueContainingOrSkillsNameContaining() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllByDescriptionsValueContainingOrSkillsNameContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", "Spring Boot", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.of(0, 10), "Test", "Spring Boot");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAlExistingJobslByPageableAndTitle() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllByTitlesValueContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllByTitlesValueContainingAndNoBadWords(PageRequest.of(0, 10), "Test");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAlExistingJobslByPageableAndDescription() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllByDescriptionsValueContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllByDescriptionsValueContainingAndNoBadWords(PageRequest.of(0, 10), "Test");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAlExistingJobslByPageableAndSkill() {
    JobEntity jobEntity1 = JobEntityMother.complete().build();
    JobEntity jobEntity2 = JobEntityMother.complete().build();
    Job job1 = JobMother.complete().id(jobEntity1.getId().toString()).build();
    Job job2 = JobMother.complete().id(jobEntity2.getId().toString()).build();
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAllBySkillsNameContainingAndBadWordFlag(
            PageRequest.of(0, 10), "Test", false))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result =
        jobAdapter.findAllBySkillsNameContainingAndNoBadWords(PageRequest.of(0, 10), "Test");

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void updateExistingJob() {
    JobEntity jobEntity = JobEntityMother.complete().build();
    Job job = JobMother.complete().id(jobEntity.getId().toString()).build();

    when(jobRepository.findById(jobEntity.getId())).thenReturn(Optional.of(jobEntity));
    when(addressMapper.toEntity(any())).thenReturn(jobEntity.getAddress());
    when(translationMapper.toEntitySet(job.getTitles())).thenReturn(jobEntity.getTitles());
    when(translationMapper.toEntitySet(job.getDescriptions())).thenReturn(jobEntity.getTitles());
    when(jobTypeMapper.toEntity(any())).thenReturn(jobEntity.getJobType());
    when(languageSkillMapper.toEntitySet(any())).thenReturn(jobEntity.getLanguages());
    when(skillMapper.toEntitySet(any())).thenReturn(jobEntity.getSkills());
    when(jobRepository.save(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(jobEntity);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(job);

    Job result = jobAdapter.update(job);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(job);
  }

  @Test
  void saveJob() {
    JobEntity jobEntity = JobEntityMother.complete().build();
    Job job =
        JobMother.complete()
            .id(jobEntity.getId().toString())
            .employerId(jobEntity.getEmployerId().toString())
            .build();

    when(jobMapper.toEntity(job)).thenReturn(jobEntity);
    when(jobRepository.save(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(jobEntity);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(job);

    Job result = jobAdapter.save(job);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(job);
  }

  @Test
  void deleteExistingJob() {
    JobEntity jobEntity = JobEntityMother.complete().build();

    jobAdapter.delete(jobEntity.getId().toString());

    verify(jobRepository).deleteById(jobEntity.getId());
  }

  private static class JobMatcher implements ArgumentMatcher<Job> {
    private final Job expected;

    public JobMatcher(Job expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(Job actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getTitles(), expected.getTitles())
          && Objects.equals(actual.getDescriptions(), expected.getDescriptions())
          && Objects.equals(actual.getJobType(), expected.getJobType())
          && Objects.equals(actual.getApplicationLink(), expected.getApplicationLink())
          && Objects.equals(actual.getApplicationMail(), expected.getApplicationMail())
          && Objects.equals(actual.getVideoUrl(), expected.getVideoUrl())
          && Objects.equals(actual.getAddress(), expected.getAddress());
    }
  }

  private static class JobEntityMatcher implements ArgumentMatcher<JobEntity> {
    private final JobEntity expected;

    public JobEntityMatcher(JobEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(JobEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getTitles(), expected.getTitles())
          && Objects.equals(actual.getDescriptions(), expected.getDescriptions())
          && Objects.equals(actual.getJobType(), expected.getJobType())
          && Objects.equals(actual.getApplicationLink(), expected.getApplicationLink())
          && Objects.equals(actual.getApplicationMail(), expected.getApplicationMail())
          && Objects.equals(actual.getVideoUrl(), expected.getVideoUrl())
          && Objects.equals(actual.getAddress(), expected.getAddress());
    }
  }
}
