package com.jobaidukraine.core.adapter.out.db.implementation.user;

import static org.assertj.core.api.Assertions.assertThat;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntityMother;
import com.jobaidukraine.core.adapter.out.db.mapper.AddressMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.RoleMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.UserMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import com.jobaidukraine.core.domain.UserMother;
import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.Optional;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MariaDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Tag("integration")
@ActiveProfiles("local")
@Transactional
@Testcontainers
public class UserAdapterIntegrationTest {

  @Autowired UserPort userPort;
  @Autowired UserRepository userRepository;
  @Autowired UserMapper userMapper;
  @Autowired AddressMapper addressMapper;
  @Autowired RoleMapper roleMapper;

  String FIX_UUID = "00000000-0000-0000-0000-000000000000";

  @Container
  private static final MariaDBContainer<?> database = new MariaDBContainer<>("mariadb:latest");

  @DynamicPropertySource
  public static void overrideProps(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", database::getJdbcUrl);
    registry.add("spring.datasource.username", database::getUsername);
    registry.add("spring.datasource.password", database::getPassword);
  }

  @Test
  @DisplayName("try to save User, with valid input")
  void saveTest1() {
    // arrange
    User user = UserMother.completeAdmin().build();

    // act
    User result = userPort.save(user);

    // assert
    assertThat(result).isNotNull();
    assertThat(result.getEmail()).isEqualTo(user.getEmail());
  }

  @Test
  @DisplayName("try to find existing user by id")
  void findByIdTest1() {
    // arrange
    // arrange
    UserEntity saved = (UserEntity) userRepository.save(UserEntityMother.completeAdmin().build());

    // act
    Optional<User> result = userPort.findById(saved.getId().toString());

    // assert
    assertThat(result).isPresent();
    assertThat(result.get().getId()).isEqualTo(saved.getId().toString());
    assertThat(result.get().getAddress()).isEqualTo(addressMapper.toDomain(saved.getAddress()));
    assertThat(result.get().getEmail()).isEqualTo(saved.getEmail());
    assertThat(result.get().getOriginCountry()).isEqualTo(saved.getOriginCountry());
    assertThat(result.get().getFirstname()).isEqualTo(saved.getFirstname());
    assertThat(result.get().getLastname()).isEqualTo(saved.getLastname());
    assertThat(result.get().getDateOfBirth()).isEqualTo(saved.getDateOfBirth());
    assertThat(result.get().getRole()).isEqualTo(roleMapper.toDomain(saved.getRole()));
  }

  @Test
  @DisplayName("try to find non existing user by id")
  void findByIdTest2() {
    // arrange & act
    Optional<User> result = userPort.findById(FIX_UUID);

    // assert
    assertThat(result).isNotPresent();
  }

  @Test
  @DisplayName("try to find existing user by email")
  void findByEmailTest1() {
    // arrange
    UserEntity saved = (UserEntity) userRepository.save(UserEntityMother.completeAdmin().build());

    // act
    Optional<User> result = userPort.findByEmail(saved.getEmail());

    // assert
    assertThat(result).isPresent();
    assertThat(result.get().getId()).isEqualTo(saved.getId().toString());
    assertThat(result.get().getAddress()).isEqualTo(addressMapper.toDomain(saved.getAddress()));
    assertThat(result.get().getEmail()).isEqualTo(saved.getEmail());
    assertThat(result.get().getOriginCountry()).isEqualTo(saved.getOriginCountry());
    assertThat(result.get().getFirstname()).isEqualTo(saved.getFirstname());
    assertThat(result.get().getLastname()).isEqualTo(saved.getLastname());
    assertThat(result.get().getDateOfBirth()).isEqualTo(saved.getDateOfBirth());
    assertThat(result.get().getRole()).isEqualTo(roleMapper.toDomain(saved.getRole()));
  }

  @Test
  @DisplayName("try to find non existing user by email")
  void findByEmailTest2() {
    // arrange & act
    Optional<User> result = userPort.findByEmail("luke@skywalker.io");

    // assert
    assertThat(result).isNotPresent();
  }

  @Test
  @DisplayName("find all users by pageable when users exist")
  void findAllByPageableTest1() {
    // arrange
    UserEntity user = (UserEntity) userRepository.save(UserEntityMother.completeAdmin().build());

    // act
    Page<User> result = userPort.findAllByPageable(PageRequest.ofSize(1));

    // assert
    assertThat(result).hasSize(1);
    assertThat(result.getContent().get(0).getId()).isEqualTo(user.getId().toString());
  }

  @Test
  @DisplayName("find all users by pageable when no users exist")
  void findAllByPageableTest2() {
    // arrange & act
    Page<User> result = userPort.findAllByPageable(PageRequest.ofSize(1));

    // assert
    assertThat(result).isEmpty();
  }

  @Test
  @DisplayName("update user with valid input")
  void updateTest1() {
    // arrange
    UserEntity user = (UserEntity) userRepository.save(UserEntityMother.completeAdmin().build());
    User toUpdate =
        UserMother.completeAdmin().id(user.getId().toString()).originCountry("France").build();

    // act
    User result = userPort.update(toUpdate);

    // assert
    assertThat(result.getId()).isEqualTo(toUpdate.getId());
    assertThat(result.getOriginCountry()).isEqualTo(toUpdate.getOriginCountry());
  }

  @Test
  @DisplayName("update user with invalid input")
  void updateTest2() {
    // arrange
    UserEntity user = (UserEntity) userRepository.save(UserEntityMother.completeAdmin().build());
    User toUpdate =
        UserMother.completeAdmin().id(user.getId().toString()).originCountry(null).build();

    // act
    User result = userPort.update(toUpdate);

    // assert
    assertThat(result.getOriginCountry()).isEqualTo("Tatooine");
    assertThat(result.getId()).isEqualTo(toUpdate.getId());
    assertThat(result.getOriginCountry()).isNotEqualTo(toUpdate.getOriginCountry());
  }

  @Test
  @DisplayName("delete existing user")
  void deleteTest1() {
    // arrange
    UserEntity user = UserEntityMother.completeAdmin().build();
    UserEntity saved = (UserEntity) userRepository.save(user);

    // act
    userPort.delete(saved.getId().toString());

    // assert
    assertThat(userRepository.findById(user.getId())).isNotPresent();
  }

  @Test
  @DisplayName("try to delete non existing user")
  void deleteTest2() {
    // arrange & act & assert
    Assert.assertThrows(EmptyResultDataAccessException.class, () -> userPort.delete(FIX_UUID));
  }
}
