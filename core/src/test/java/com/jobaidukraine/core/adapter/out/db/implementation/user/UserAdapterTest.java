package com.jobaidukraine.core.adapter.out.db.implementation.user;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntityMother;
import com.jobaidukraine.core.adapter.out.db.mapper.AddressMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.RoleMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.UserMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import com.jobaidukraine.core.domain.UserMother;
import com.jobaidukraine.core.domain.entities.User;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class UserAdapterTest {

  @Mock UserRepository userRepository;
  @Mock UserMapper userMapper;
  @Mock RoleMapper roleMapper;
  @Mock AddressMapper addressMapper;
  @InjectMocks UserAdapter userAdapter;

  @Test
  void findExistingUserById() {
    UserEntity userEntity = UserEntityMother.completeAdmin().build();
    User user = UserMother.completeAdmin().id(userEntity.getId().toString()).build();

    when(userRepository.findById(userEntity.getId())).thenReturn(Optional.of(userEntity));
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    Optional<User> result = userAdapter.findById(userEntity.getId().toString());

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void findAllExistingUsersByPageable() {
    UserEntity userEntity1 = UserEntityMother.completeAdmin().build();
    UserEntity userEntity2 = UserEntityMother.completeAdmin().build();
    User user1 = UserMother.completeAdmin().id(userEntity1.getId().toString()).build();
    User user2 = UserMother.completeAdmin().id(userEntity2.getId().toString()).build();
    Page<UserEntity> userEntityPage = new PageImpl<>(List.of(userEntity1, userEntity2));

    when(userRepository.findAll(PageRequest.of(0, 10))).thenReturn(userEntityPage);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity1)))).thenReturn(user1);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity2)))).thenReturn(user2);

    Page<User> result = userAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(user1, user2));
  }

  @Test
  void findExistingUsersByEmail() {
    UserEntity userEntity = UserEntityMother.completeAdmin().build();
    User user = UserMother.completeAdmin().id(userEntity.getId().toString()).build();

    when(userRepository.findByEmail(userEntity.getEmail())).thenReturn(Optional.of(userEntity));
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    Optional<User> result = userAdapter.findByEmail(userEntity.getEmail());

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void saveUser() {
    UserEntity userEntity = UserEntityMother.completeAdmin().build();
    UserEntity userEntityNull = UserEntityMother.completeAdmin().id(null).build();
    User user = UserMother.completeAdmin().id(userEntity.getId().toString()).build();
    User userNull = UserMother.completeAdmin().id(null).build();

    when(userMapper.toEntity(argThat(new UserMatcher(userNull)))).thenReturn(userEntityNull);
    when(userRepository.save(argThat(new UserEntityMatcher(userEntityNull))))
        .thenReturn(userEntity);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    User result = userAdapter.save(userNull);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void updateExistingUser() {
    UserEntity userEntity = UserEntityMother.completeAdmin().build();
    User user = UserMother.completeAdmin().id(userEntity.getId().toString()).build();

    when(userRepository.findById(userEntity.getId())).thenReturn(Optional.of(userEntity));
    when(roleMapper.toEntity(any())).thenReturn(userEntity.getRole());
    when(addressMapper.toEntity(any())).thenReturn(userEntity.getAddress());
    when(userRepository.save(argThat(new UserEntityMatcher(userEntity)))).thenReturn(userEntity);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    User result = userAdapter.update(user);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void deleteExistingUser() {
    UserEntity userEntity = UserEntityMother.completeAdmin().build();

    userAdapter.delete(userEntity.getId().toString());

    verify(userRepository).deleteById(userEntity.getId());
  }

  private static class UserMatcher implements ArgumentMatcher<User> {
    private final User expected;

    public UserMatcher(User expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(User actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(expected.getId(), actual.getId())
          && Objects.equals(expected.getCreatedAt(), actual.getCreatedAt())
          && Objects.equals(expected.getUpdatedAt(), actual.getUpdatedAt())
          && Objects.equals(expected.getEmail(), actual.getEmail())
          && Objects.equals(expected.getFirstname(), actual.getFirstname())
          && Objects.equals(expected.getLastname(), actual.getLastname())
          && Objects.equals(expected.getRole(), actual.getRole());
    }
  }

  private static class UserEntityMatcher implements ArgumentMatcher<UserEntity> {
    private final UserEntity expected;

    public UserEntityMatcher(UserEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(UserEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(expected.getId(), actual.getId())
          && Objects.equals(expected.getCreatedAt(), actual.getCreatedAt())
          && Objects.equals(expected.getUpdatedAt(), actual.getUpdatedAt())
          && Objects.equals(expected.getEmail(), actual.getEmail())
          && Objects.equals(expected.getFirstname(), actual.getFirstname())
          && Objects.equals(expected.getLastname(), actual.getLastname())
          && Objects.equals(expected.getRole(), actual.getRole());
    }
  }
}
