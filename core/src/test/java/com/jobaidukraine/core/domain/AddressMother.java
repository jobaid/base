package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.Address;
import java.util.Set;

public class AddressMother {
  public static Address.AddressBuilder germanAddress() {
    return Address.builder()
        .latitude(51.5109688f)
        .longitude(7.4660812f)
        .street("Friedensplatz")
        .houseNumber("1337")
        .zip("44315")
        .cityTranslations(Set.of(TranslationMother.germanTranslation().value("Dortmund").build()))
        .country("DE");
  }
}
