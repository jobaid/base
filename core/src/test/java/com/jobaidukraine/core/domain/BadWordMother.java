package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.BadWord;
import java.util.Set;
import java.util.UUID;

public class BadWordMother {
  public static BadWord.BadWordBuilder germanBadWord() {
    return BadWord.builder()
        .id(UUID.randomUUID().toString())
        .wordTranslations(
            Set.of(TranslationMother.germanTranslation().value("Schimpfwort").build()));
  }

  public static BadWord.BadWordBuilder englishBadWord() {
    return BadWord.builder()
        .id(UUID.randomUUID().toString())
        .wordTranslations(
            Set.of(TranslationMother.englishTranslation().value("Swear word").build()));
  }

  public static BadWord.BadWordBuilder multilingualBadWord() {
    return BadWord.builder()
        .id(UUID.randomUUID().toString())
        .wordTranslations(
            Set.of(
                TranslationMother.germanTranslation().value("Schimpfwort").build(),
                TranslationMother.englishTranslation().value("Swear word").build()));
  }
}
