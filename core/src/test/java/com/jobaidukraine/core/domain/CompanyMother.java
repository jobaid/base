package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.Company;

public class CompanyMother {
  public static Company.CompanyBuilder germanCompany() {
    return Company.builder()
        .name("Adesso")
        .url("www.adesso.de")
        .email("info@adesso.de")
        .logo("test-logo");
  }
}
