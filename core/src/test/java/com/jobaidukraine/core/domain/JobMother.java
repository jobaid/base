package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.domain.values.JobType;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class JobMother {
  public static Job.JobBuilder complete() {
    return Job.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .address(AddressMother.germanAddress().build())
        .badWordFlag(false)
        .titles(Set.of(TranslationMother.germanTranslation().value("Test").build()))
        .descriptions(Set.of(TranslationMother.germanTranslation().value("Test").build()))
        .jobType(JobType.FREELANCE)
        .applicationLink("https://jobaidukraine.com/apply")
        .applicationMail("apply@jobaidukraine.com")
        .videoUrl("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
        .languages(Set.of(LanguageSkillMother.germanFluent().build()))
        .skills(Set.of(SkillMother.professionalSkill().name("Spring Boot").build()));
  }
}
