package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.LanguageLevel;
import com.jobaidukraine.core.domain.values.LanguageSkill;
import java.util.Locale;

public class LanguageSkillMother {
  public static LanguageSkill.LanguageSkillBuilder germanFluent() {
    return LanguageSkill.builder().locale(Locale.GERMAN).level(LanguageLevel.FLUENT);
  }
}
