package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.entities.Refugee;
import com.jobaidukraine.core.domain.values.Role;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class RefugeeMother {
  public static Refugee.RefugeeBuilder complete() {
    return Refugee.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(Role.REFUGEE)
        .address(AddressMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .languages(Set.of(LanguageSkillMother.germanFluent().build()))
        .skills(Set.of(SkillMother.professionalSkill().name("Spring Boot").build()));
  }
}
