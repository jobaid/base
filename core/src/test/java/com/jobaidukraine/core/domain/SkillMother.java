package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.Skill;
import com.jobaidukraine.core.domain.values.SkillLevel;

public class SkillMother {
  public static Skill.SkillBuilder professionalSkill() {
    return Skill.builder().name("Professional").level(SkillLevel.PROFESSIONAL);
  }
}
