package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.values.Translation;
import com.jobaidukraine.core.domain.values.TranslationType;
import java.time.LocalDateTime;
import java.util.Locale;

public class TranslationMother {
  public static Translation.TranslationBuilder germanTranslation() {
    return Translation.builder()
        .locale(Locale.GERMAN)
        .type(TranslationType.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }

  public static Translation.TranslationBuilder englishTranslation() {
    return Translation.builder()
        .locale(Locale.ENGLISH)
        .type(TranslationType.ORIGINAL_TRANSLATION)
        .translatedOn(LocalDateTime.of(2021, 12, 24, 11, 10))
        .translatedBy("John Doe");
  }
}
