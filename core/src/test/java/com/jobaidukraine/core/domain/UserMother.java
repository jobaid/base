package com.jobaidukraine.core.domain;

import com.jobaidukraine.core.domain.entities.Employer;
import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.domain.values.Role;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

public class UserMother {
  public static Employer.EmployerBuilder completeEmployer() {
    return Employer.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(Role.EMPLOYER)
        .address(AddressMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine")
        .company(CompanyMother.germanCompany().build())
        .jobs(Set.of(JobMother.complete().build()));
  }

  public static User.UserBuilder completeAdmin() {
    return User.builder()
        .id(UUID.randomUUID().toString())
        .createdAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .updatedAt(LocalDateTime.of(2021, 12, 24, 11, 10))
        .role(Role.ADMIN)
        .address(AddressMother.germanAddress().build())
        .email("luke@skywalker.io")
        .firstname("Luke")
        .lastname("Skywalker")
        .dateOfBirth(LocalDate.of(2021, 12, 24))
        .originCountry("Tatooine");
  }
}
