package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.BadWordMother;
import com.jobaidukraine.core.domain.TranslationMother;
import com.jobaidukraine.core.domain.values.BadWord;
import com.jobaidukraine.core.services.ports.out.BadWordPort;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class BadWordServiceTest {

  @Mock private BadWordPort badWordPort;
  @InjectMocks private BadWordService badWordService;

  @Test
  void checkTranslationForBadWordsContainingBadWord() {
    Mockito.when(badWordPort.findAll())
        .thenReturn(List.of(BadWordMother.multilingualBadWord().build()));

    Assertions.assertThat(
            badWordService.checkTranslationForBadWords(
                TranslationMother.germanTranslation()
                    .value("Text, der ein Schimpfwort enthält")
                    .build()))
        .isFalse();
  }

  @Test
  void checkTranslationForBadWordsContainingBadWordInDifferentLanguage() {
    Mockito.when(badWordPort.findAll()).thenReturn(List.of(BadWordMother.germanBadWord().build()));

    Assertions.assertThat(
            badWordService.checkTranslationForBadWords(
                TranslationMother.englishTranslation()
                    .value("Text, der ein Schimpfwort enthält")
                    .build()))
        .isFalse();
  }

  @Test
  void checkTranslationForBadWords() {
    Mockito.when(badWordPort.findAll())
        .thenReturn(List.of(BadWordMother.multilingualBadWord().build()));

    Assertions.assertThat(
            badWordService.checkTranslationForBadWords(
                TranslationMother.germanTranslation().value("Text, der in Ordnung ist").build()))
        .isTrue();
  }

  @Test
  void findById() {
    BadWord badWord =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();
    Mockito.when(badWordPort.findById("00000000-0000-0000-0000-000000000000"))
        .thenReturn(Optional.of(badWord));

    BadWord result = badWordService.findById("00000000-0000-0000-0000-000000000000");

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(badWord);
  }

  @Test
  void findByIdNotFound() {
    Mockito.when(badWordPort.findById("00000000-0000-0000-0000-000000000000"))
        .thenReturn(Optional.empty());

    Assertions.assertThatThrownBy(
            () -> badWordService.findById("00000000-0000-0000-0000-000000000000"))
        .isInstanceOf(NoSuchElementException.class);
  }

  @Test
  void findAll() {
    Page<BadWord> badWords =
        new PageImpl(
            List.of(
                BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build(),
                BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000001").build()));

    Mockito.when(badWordPort.findAll(Pageable.ofSize(2))).thenReturn(badWords);

    Page<BadWord> result = badWordService.findAll(Pageable.ofSize(2));

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(badWords);
  }

  @Test
  void saveBadWord() {
    BadWord badWord = BadWordMother.germanBadWord().id(null).build();
    BadWord saved =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();

    Mockito.when(badWordPort.saveBadWord(badWord)).thenReturn(saved);

    BadWord result = badWordService.saveBadWord(badWord);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(saved);
  }

  @Test
  void updateBadWord() {
    BadWord badWord =
        BadWordMother.germanBadWord()
            .id("00000000-0000-0000-0000-000000000000")
            .wordTranslations(Collections.emptySet())
            .build();
    BadWord saved =
        BadWordMother.germanBadWord().id("00000000-0000-0000-0000-000000000000").build();
    Mockito.when(badWordPort.updateBadWord(badWord)).thenReturn(saved);

    BadWord result = badWordService.updateBadWord(badWord);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(saved);
  }

  @Test
  void deleteBadWord() {
    badWordService.deleteBadWord("00000000-0000-0000-0000-000000000000");
    Mockito.verify(badWordPort).deleteBadWord("00000000-0000-0000-0000-000000000000");
  }
}
