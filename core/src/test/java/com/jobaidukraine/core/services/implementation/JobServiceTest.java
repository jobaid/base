package com.jobaidukraine.core.services.implementation;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.JobMother;
import com.jobaidukraine.core.domain.entities.Job;
import com.jobaidukraine.core.domain.values.Translation;
import com.jobaidukraine.core.services.ports.in.BadWordUseCase;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class JobServiceTest {

  @Mock private JobPort jobPort;
  @Mock private BadWordUseCase badWordUseCase;
  @InjectMocks private JobService jobService;

  @Test
  void saveJob() {
    Job job = JobMother.complete().build();
    when(jobPort.save(job)).thenReturn(job);
    when(badWordUseCase.checkTranslationForBadWords(ArgumentMatchers.any(Translation.class)))
        .thenReturn(true);

    Job result = jobService.save(job, "");

    assertThat(result).isEqualTo(job);
  }

  @Test
  void saveJobWithBadWord() {
    Job job = JobMother.complete().id("00000000-0000-0000-0000-000000000000").build();
    Job jobWithBadWord =
        JobMother.complete()
            .id("00000000-0000-0000-0000-000000000000")
            .badWordFlag(true)
            .employerId("")
            .build();

    when(badWordUseCase.checkTranslationForBadWords(ArgumentMatchers.any(Translation.class)))
        .thenReturn(false);
    when(jobPort.save(job)).thenReturn(job);

    Job result = jobService.save(job, "");

    assertThat(result).isEqualTo(jobWithBadWord);
  }

  @Test
  @DisplayName("findAllByPageable without specific title")
  void findAllByPageableWithBadWords() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().badWordFlag(true).build()));
    when(jobPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(job);

    Page<Job> result = jobService.findAllByPageable(PageRequest.of(0, 1));

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable without specific title")
  void findAllByPageable() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(job);

    Page<Job> result = jobService.findAllByPageable(PageRequest.of(0, 1));

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific title, description and skill")
  void findAllByPageableTitleDescriptionSkill() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                Pageable.ofSize(1), "Test", "Test", "Spring Boot"))
        .thenReturn(job);

    Page<Job> result =
        jobService
            .findAllByTitlesValueContainingOrDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
                PageRequest.of(0, 1), "Test", "Test", "Spring Boot");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific title and description")
  void findAllByPageableTitleDescription() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            Pageable.ofSize(1), "Test", "Spring Boot"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllByTitlesValueContainingOrDescriptionsValueContainingAndNoBadWords(
            PageRequest.of(0, 1), "Test", "Spring Boot");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific title and skill")
  void findAllByPageableTitleSkill() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            Pageable.ofSize(1), "Test", "Spring Boot"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllByTitlesValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.of(0, 1), "Test", "Spring Boot");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific description and skill")
  void findAllByPageableDescriptionSkill() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            Pageable.ofSize(1), "Test", "Spring Boot"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllByDescriptionsValueContainingOrSkillsNameContainingAndNoBadWords(
            PageRequest.of(0, 1), "Test", "Spring Boot");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific title")
  void findAllByPageableTitle() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByTitlesValueContainingAndNoBadWords(Pageable.ofSize(1), "Test"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllByTitlesValueContainingAndNoBadWords(PageRequest.of(0, 1), "Test");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific description")
  void findAllByPageableDescription() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllByDescriptionsValueContainingAndNoBadWords(Pageable.ofSize(1), "Test"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllByDescriptionsValueContainingAndNoBadWords(PageRequest.of(0, 1), "Test");

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("findAllByPageable with specific skill")
  void findAllByPageableSkill() {
    Page<Job> job = new PageImpl<>(List.of(JobMother.complete().build()));
    when(jobPort.findAllBySkillsNameContainingAndNoBadWords(Pageable.ofSize(1), "Spring Boot"))
        .thenReturn(job);

    Page<Job> result =
        jobService.findAllBySkillsNameContainingAndNoBadWords(PageRequest.of(0, 1), "Spring Boot");

    assertThat(result).isEqualTo(job);
  }

  @Test
  void findById() {
    Job job = JobMother.complete().build();
    when(jobPort.findById(job.getId())).thenReturn(Optional.of(job));

    Job result = jobService.findById(job.getId());

    assertThat(result).isEqualTo(job);
  }

  @Test
  @DisplayName("no job for id \"00000000-0000-0000-0000-000000000000\" found")
  void findById2() {
    when(jobPort.findById("00000000-0000-0000-0000-000000000000"))
        .thenThrow(new NoSuchElementException());

    Assertions.assertThatThrownBy(() -> jobService.findById("00000000-0000-0000-0000-000000000000"))
        .isInstanceOf(NoSuchElementException.class);
  }

  @Test
  void update() {
    Job base = JobMother.complete().build();
    Job update = JobMother.complete().build();
    update.setBadWordFlag(true);
    when(jobPort.update(base)).thenReturn(update);

    Job result = jobService.update(base);

    assertThat(result).isEqualTo(update);
  }

  @Test
  void updateJobWithBadWord() {
    Job job = JobMother.complete().id("00000000-0000-0000-0000-000000000000").build();
    Job jobWithBadWord =
        JobMother.complete().id("00000000-0000-0000-0000-000000000000").badWordFlag(true).build();

    when(badWordUseCase.checkTranslationForBadWords(ArgumentMatchers.any(Translation.class)))
        .thenReturn(false);
    when(jobPort.update(job)).thenReturn(job);

    Job result = jobService.update(job);

    assertThat(result).isEqualTo(jobWithBadWord);
  }

  @Test
  void delete() {
    Job job = JobMother.complete().build();

    jobService.delete(job.getId());

    verify(jobPort).delete(job.getId());
  }
}
