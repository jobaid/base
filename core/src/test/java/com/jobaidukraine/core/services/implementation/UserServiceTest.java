package com.jobaidukraine.core.services.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.UserMother;
import com.jobaidukraine.core.domain.entities.User;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
  @Mock private UserPort userPort;

  @InjectMocks private UserService userService;

  @Test
  void saveUser() {
    User user = getMockUser();
    when(userPort.save(user)).thenReturn(user);

    User result = userService.save(user);
    assertEquals(result, user);
  }

  @Test
  void findAllByPageable() {
    Page<User> user = new PageImpl<>(List.of(getMockUser()));

    when(userPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(user);

    Page<User> result = userService.findAllByPageable(PageRequest.of(0, 1));

    assertEquals(result, user);
  }

  @Test
  void findById() {
    User user = getMockUser();

    when(userPort.findById(user.getId())).thenReturn(Optional.of(user));

    User result = userService.findById(user.getId());
    assertEquals(result, user);
  }

  @Test
  @DisplayName("no user for id \"00000000-0000-0000-0000-000000000000\" found")
  void findById2() {
    when(userPort.findById("00000000-0000-0000-0000-000000000000"))
        .thenThrow(new NoSuchElementException());

    assertThrows(
        NoSuchElementException.class,
        () -> userService.findById("00000000-0000-0000-0000-000000000000"));
  }

  @Test
  void findByEmail() {
    Optional<User> user = Optional.of(getMockUser());

    when(userPort.findByEmail("test@test.test")).thenReturn(user);

    Optional<User> result = userService.findByEmail("test@test.test");
    assertEquals(result, user);
  }

  @Test
  void update() {
    User base = getMockUser();
    User update = getMockUser();
    update.setOriginCountry("germany");

    when(userPort.update(base)).thenReturn(update);

    User result = userService.update(base);
    assertEquals(result, update);
  }

  @Test
  void delete() {
    User user = getMockUser();
    userService.delete(user.getId());
    verify(userPort).delete(user.getId());
  }

  User getMockUser() {
    return UserMother.completeAdmin().build();
  }
}
